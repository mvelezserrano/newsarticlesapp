//
//  HTTPClient.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 19/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public protocol HTTPClientTask {
    func cancel()
}

public protocol HTTPClient {
    typealias Result = Swift.Result<(Data, HTTPURLResponse), Error>
    
    func post(with request: URLRequest, completion: @escaping (Result) -> Void)
    @discardableResult
    func get(from url: URL, completion: @escaping (Result) -> Void) -> HTTPClientTask
}

public struct AuthRequest {
    var url: URL
    var completion: (HTTPClient.Result) -> Void
}

protocol HTTPClientRefreshToken {
    var pendingRequests: [AuthRequest] { get set }
    func safelyAddRequest(request: AuthRequest)
    func safelySendAllRequests()
    func refreshToken(completion: @escaping (HTTPClient.Result) -> Void)
}

