//
//  LoginUIComposer.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

public final class LoginUIComposer {
    private init() {}
    
    public static func loginComposedWith(authService: AuthServiceProtocol, keychainStore: KeychainStore) -> LoginViewController {
        let interactor = LoginInteractor(authService: MainQueueDispatchDecorator(decoratee: authService), keychainStore: keychainStore)
        let viewController = makeLoginViewController(interactor: interactor)
        
        interactor.presenter = LoginPresenter(
            loginView: viewController,
            loadingView: WeakRefVirtualProxy(viewController),
            errorView: WeakRefVirtualProxy(viewController))
        
        return viewController
    }
    
    private static func makeLoginViewController(interactor: LoginInteractor) -> LoginViewController {
        let bundle = Bundle(for: LoginViewController.self)
        let storyboard = UIStoryboard(name: "Login", bundle: bundle)
        let controller = storyboard.instantiateInitialViewController() as! LoginViewController
        controller.interactor = interactor
        return controller
    }
}
