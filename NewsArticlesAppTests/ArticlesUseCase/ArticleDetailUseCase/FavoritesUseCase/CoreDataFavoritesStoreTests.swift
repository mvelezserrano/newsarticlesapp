//
//  CoreDataFavoritesStoreTests.swift
//  NewsArticlesAppTests
//
//  Created by Miguel Angel Vélez Serrano on 24/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import XCTest

class CoreDataFavoritesStoreTests: XCTestCase {
    
    func test_retrieve_deliversNoFavoritesOnEmptyFavorites() {
        let sut = makeSUT()
        
        expect(sut, toRetrieve: .success([]))
    }
    
    func test_retrieve_hasNoSideEffectsOnEmptyCache() {
        let sut = makeSUT()
        
        expect(sut, toRetrieveTwice: .success([]))
    }
    
    func test_retrieve_deliversFoundValuesOnNonEmptyFavorites() {
        let sut = makeSUT()
        
        let favorite = 99
        insert(favorite, to: sut)
        
        expect(sut, toRetrieve: .success([favorite]))
    }
    
    func test_retrieve_hasNoSideEffectsOnNonEmptyFavorites() {
        let sut = makeSUT()
        
        let favorite = 99
        insert(favorite, to: sut)
        
        expect(sut, toRetrieveTwice: .success([favorite]))
    }
    
    func test_insert_deliversNoErrorOnEmptyFavorites() {
        let sut = makeSUT()
        
        let insertionError = insert(99, to: sut)
        
        XCTAssertNil(insertionError, "Expected to insert favorite successfully")
    }
    
    func test_insert_deliversNoErrorOnNonEmptyFavorites() {
        let sut = makeSUT()
        insert(10, to: sut)
        
        let insertionError = insert(99, to: sut)
        
        XCTAssertNil(insertionError, "Expected to insert favorite successfully")
    }
    
    func test_retrieve_deliversAllValuesAfterFavoriteInsertion() {
        let sut = makeSUT()
        
        let favorite1 = 99
        let favorite2 = 10
        insert(favorite1, to: sut)
        insert(favorite2, to: sut)
        
        expect(sut, toRetrieveTwice: .success([favorite1, favorite2]))
    }
    
    func test_delete_deliversNoErrorOnEmptyFavorites() {
        let sut = makeSUT()
        
        let deletionError = delete(99, from: sut)
        
        XCTAssertNil(deletionError, "Expected no error on deletion empty favorites")
    }
    
    func test_delete_hasNoSideEffectsOnEmptyFavorites() {
        let sut = makeSUT()
        
        delete(99, from: sut)
        
        expect(sut, toRetrieveTwice: .success([]))
    }
    
    func test_delete_deliversNoErrorOnNonEmptyFavorites() {
        let sut = makeSUT()
        insert(99, to: sut)
        
        let deletionError = delete(99, from: sut)
        
        XCTAssertNil(deletionError, "Expected to delete favorite successfully")
    }
    
    func test_storeSideEffects_runSerially() {
        let sut = makeSUT()
        
        var completedOperationsInOrder = [XCTestExpectation]()
        
        let op1 = expectation(description: "Operation 1")
        sut.insert(favoriteId: 99) { _ in
            completedOperationsInOrder.append(op1)
            op1.fulfill()
        }
        
        let op2 = expectation(description: "Operation 2")
         sut.delete(favoriteId: 99) { _ in
            completedOperationsInOrder.append(op2)
            op2.fulfill()
        }
        
        let op3 = expectation(description: "Operation 3")
        sut.insert(favoriteId: 99) { _ in
            completedOperationsInOrder.append(op3)
            op3.fulfill()
        }
        
        waitForExpectations(timeout: 5.0)
        
        XCTAssertEqual(completedOperationsInOrder, [op1, op2, op3], "Expected side-effects to run serially but operations finished in the wrong order")
    }

    // - MARK: Helpers
    
    private func makeSUT(file: StaticString = #file, line: UInt = #line) -> FavoritesStore {
        let storeURL = URL(fileURLWithPath: "/dev/null")
        let sut = try! CoreDataStore(storeURL: storeURL)
        trackForMemoryLeaks(sut, file: file, line: line)
        return sut
    }

    @discardableResult
    func insert(_ favoriteId: Int, to sut: FavoritesStore) -> Error? {
        let exp = expectation(description: "Wait for favorite insertion")
        var insertionError: Error?
        sut.insert(favoriteId: favoriteId) { result in
            if case let Result.failure(error) = result { insertionError = error }
            exp.fulfill()
        }
        wait(for: [exp], timeout: 1.0)
        return insertionError
    }
    
    @discardableResult
    func delete(_ favoriteId: Int, from sut: FavoritesStore) -> Error? {
        let exp = expectation(description: "Wait for cache deletion")
        var deletionError: Error?
        sut.delete(favoriteId: favoriteId) { result in
            if case let Result.failure(error) = result { deletionError = error }
            exp.fulfill()
        }
        wait(for: [exp], timeout: 1.0)
        return deletionError
    }
    
    @discardableResult
    func isFavorite(_ favoriteId: Int, in sut: FavoritesStore) -> Error? {
        let exp = expectation(description: "Wait for cache deletion")
        var validationError: Error?
        sut.isFavorite(favoriteId: favoriteId) { result in
            if case let Result.failure(error) = result { validationError = error }
            exp.fulfill()
        }
        wait(for: [exp], timeout: 1.0)
        return validationError
    }
    
    func expect(_ sut: FavoritesStore, toRetrieveTwice expectedResult: FavoritesStore.RetrievalResult, file: StaticString = #file, line: UInt = #line) {
        expect(sut, toRetrieve: expectedResult, file: file, line: line)
        expect(sut, toRetrieve: expectedResult, file: file, line: line)
    }
    
    func expect(_ sut: FavoritesStore, toRetrieve expectedResult: FavoritesStore.RetrievalResult, file: StaticString = #file, line: UInt = #line) {
        let exp = expectation(description: "Wait for cache retrieval")
        
        sut.retrieve { retrievedResult in
            switch (expectedResult, retrievedResult) {
            case let (.success(expected), .success(retrieved)):
                XCTAssertEqual(retrieved, expected, file: file, line: line)
                
            default:
                XCTFail("Expected to retrieve \(expectedResult), got \(retrievedResult) instead", file: file, line: line)
            }
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 1.0)
    }
}
