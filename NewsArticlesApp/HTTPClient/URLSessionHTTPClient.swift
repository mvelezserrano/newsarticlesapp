//
//  URLSessionHTTPClient.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 20/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public class URLSessionHTTPClient: HTTPClient, HTTPClientRefreshToken {
    private let session: URLSession
    let keychainStore: KeychainStoreProtocol
    
    public init(session: URLSession, keychainStore: KeychainStoreProtocol) {
        self.session = session
        self.keychainStore = keychainStore
    }
    
    private struct UnexpectedValuesRepresentation: Swift.Error {}
    
    public enum Error: Swift.Error {
        case connectivity
        case invalidData
        case unauthorized
    }
    
    private struct URLSessionTaskWrapper: HTTPClientTask {
        let wrapped: URLSessionTask
        
        func cancel() {
            wrapped.cancel()
        }
    }
    
    public func post(with request: URLRequest, completion: @escaping (HTTPClient.Result) -> Void) {
        let task = session.dataTask(with: request) { data, response, error in
            completion(Result {
                if let error = error {
                    throw error
                } else if let data = data, let response = response as? HTTPURLResponse {
                    return (data, response)
                } else {
                    throw UnexpectedValuesRepresentation()
                }
            })
        }
        task.resume()
    }
    
    public func get(from url: URL, completion: @escaping (HTTPClient.Result) -> Void) -> HTTPClientTask {
        let authRequest = AuthRequest(url: url, completion: completion)
        var request = URLRequest(url: url)
        request.addValue("Bearer \(keychainStore.getAccessToken() ?? "")", forHTTPHeaderField: "Authorization")
        let task = session.dataTask(with: request) { [weak self] data, response, error in
            if let response = response as? HTTPURLResponse, response.unauthorized {
                self?.safelyAddRequest(request: authRequest)
                self?.refreshToken(completion: completion)
            } else {
                completion(Result {
                    if let error = error {
                        throw error
                    } else if let data = data, let response = response as? HTTPURLResponse {
                        return (data, response)
                    } else {
                        throw UnexpectedValuesRepresentation()
                    }
                })
            }
        }
        task.resume()
        return URLSessionTaskWrapper(wrapped: task)
    }

    // MARK: Refresh Token
    private let unauthorizedRequestsQueue = DispatchQueue( label: "Session.unauthorizedRequestsQueue", attributes: .concurrent)
    private var unAuthorizedRequests = [AuthRequest]()
    
    public var pendingRequests: [AuthRequest] {
        get { return unAuthorizedRequests }
        set { unAuthorizedRequests = newValue }
    }
    
    public func safelyAddRequest(request: AuthRequest) {
        unauthorizedRequestsQueue.async(flags: .barrier) { [weak self] in
            self?.pendingRequests.append(request)
        }
    }
    
    public func safelySendAllRequests() {
        unauthorizedRequestsQueue.async(flags: .barrier) { [weak self] in
            self?.pendingRequests.forEach { (authRequest) in
                _ = self?.get(from: authRequest.url, completion: authRequest.completion)
            }
            self?.pendingRequests.removeAll()
        }
    }
    
    public func refreshToken(completion: @escaping (HTTPClient.Result) -> Void) {
        let request = AuthService.refreshTokenRequestUsingRefreshToken(keychainStore.getRefreshToken())
        post(with: request) { [weak self] result in
            switch result {
            case let .success((data, response)):
                let tokenResult = AuthService.map(data, from: response)
                guard case let .success(token) = tokenResult else {
                    completion(Result { throw Error.unauthorized })
                    return
                }
                self?.keychainStore.storeAuthDataFromRefreshToken(token: token) { _ in
                    self?.safelySendAllRequests()
                }
            case .failure:
                completion(Result { throw Error.connectivity })
            }
        }
    }
}
