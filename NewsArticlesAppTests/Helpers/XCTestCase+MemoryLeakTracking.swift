//
//  XCTestCase+MemoryLeakTracking.swift
//  NewsArticlesAppTests
//
//  Created by Miguel Angel Vélez Serrano on 20/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import XCTest

extension XCTestCase {
    func trackForMemoryLeaks(_ instance: AnyObject, file: StaticString = #file, line: UInt = #line) {
        addTeardownBlock { [weak instance] in
            XCTAssertNil(instance, "Instance should have been deallocated. Potential memory leak.", file: file, line: line)
        }
    }
}
