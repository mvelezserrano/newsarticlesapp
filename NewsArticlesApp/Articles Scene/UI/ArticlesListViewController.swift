//
//  ArticlesListViewController.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

protocol ArticlesListRoutingDelegate: class {
    func didSelect(article: SummaryArticle, inVC vc: ArticlesListViewController)
    func didLogout(inVC vc: ArticlesListViewController)
}

final class ArticlesListViewController: UITableViewController, UITableViewDataSourcePrefetching {

    public var interactor: ArticlesListInteractor!
    public weak var routingDelegate: ArticlesListRoutingDelegate?
    
    @IBOutlet private(set) public var errorView: ErrorView?
    weak var activityIndicator: UIActivityIndicatorView!
    
    private var tableModel = [ArticleCellController]() {
        didSet { tableView.reloadData() }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        assert(routingDelegate != nil, "Routing delegate not setted")
        createActivityIndicator()
        interactor.requestArticlesList()
    }
    
    private func createActivityIndicator() {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        activityIndicator.color = Configuration.Appearance.mainColor
        tableView.backgroundView = activityIndicator
        self.activityIndicator = activityIndicator
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        tableView.sizeTableHeaderToFit()
    }
    
    @IBAction private func logout() {
        routingDelegate?.didLogout(inVC: self)
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableModel.count
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cellController(forRowAt: indexPath).view(in: tableView)
    }
    
    public override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cancelCellControllerLoad(forRowAt: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { indexPath in
            cellController(forRowAt: indexPath).preload()
        }
    }
    
    public func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach(cancelCellControllerLoad)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let article = interactor.articleAtIndex(indexPath.row) else {
            return
        }
        routingDelegate?.didSelect(article: article, inVC: self)
    }
    
    private func cellController(forRowAt indexPath: IndexPath) -> ArticleCellController {
        return tableModel[indexPath.row]
    }
    
    private func cancelCellControllerLoad(forRowAt indexPath: IndexPath) {
        cellController(forRowAt: indexPath).cancelLoad()
    }
}

extension ArticlesListViewController: ArticlesListView, LoadingView, ErrorMessageView {
    
    public func display(_ viewModel: ArticlesListViewModel) {
        tableModel = viewModel.articles.map { model in
            let summaryArticleInteractor = interactor.summaryArticleInteractor(for: model)
            let view = ArticleCellController(interactor: summaryArticleInteractor)
            summaryArticleInteractor.presenter = SummaryArticlePresenter(view: WeakRefVirtualProxy(view))

            return view
        }
    }
    
    public func display(_ viewModel: LoadingViewModel) {
        activityIndicator.update(isLoading: viewModel.isLoading)
    }
    
    public func display(_ viewModel: ErrorMessageViewModel) {
        errorView?.message = viewModel.message
    }
}
