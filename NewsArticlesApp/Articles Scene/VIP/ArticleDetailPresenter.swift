//
//  ArticleDetailPresenter.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 26/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

public protocol ArticleDetailView {
    func display(_ viewModel: ArticleDetailViewModel)
}

public final class ArticleDetailPresenter {
    private let articleDetailView: ArticleDetailView
    private let loadingView: LoadingView
    private let errorView: ErrorMessageView
    
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    public init(articleDetailView: ArticleDetailView, loadingView: LoadingView, errorView: ErrorMessageView) {
        self.articleDetailView = articleDetailView
        self.loadingView = loadingView
        self.errorView = errorView
    }
    
    private var articleLoadError: String {
        return NSLocalizedString("ARTICLES_LIST_VIEW_CONNECTION_ERROR", tableName: "Articles", bundle: Bundle(for: ArticlesListPresenter.self), comment: "Error message displayed when we can't load article detail from the server")
    }
    
    public func didStartLoadingArticle() {
        errorView.display(.noError)
        loadingView.display(LoadingViewModel(isLoading: true))
    }
    
    public func didFinishLoadingArticle(with article: Article) {
        articleDetailView.display(viewModelFromArticle(article))
    }
    
    public func didFinishLoadingArticle(with error: Error) {
        errorView.display(.error(message: articleLoadError))
        loadingView.display(LoadingViewModel(isLoading: false))
    }
    
    public func didStartLoadingData(for model: Article) {
        errorView.display(.noError)
        loadingView.display(LoadingViewModel(isLoading: true))
    }
    
    public func didFinishLoadingData(with data: Data, for model: Article) {
        loadingView.display(LoadingViewModel(isLoading: false))
        articleDetailView.display(viewModelFromArticle(model, data: data))
    }
    
    public func didFinishLoadingData(with error: Error, for model: Article) {
        errorView.display(.error(message: articleLoadError))
        loadingView.display(LoadingViewModel(isLoading: false))
    }
    
    private func viewModelFromArticle(_ article: Article, data: Data? = nil) -> ArticleDetailViewModel {
        let image = data != nil ? UIImage(data: data!) : nil
        return ArticleDetailViewModel(date: dateFormatter.string(from: article.date),
                                      title: article.title,
                                      image: image,
                                      content: article.content,
                                      sourceUrl: article.sourceUrl,
                                      isFavorite: article.isFavorite)
    }
}
