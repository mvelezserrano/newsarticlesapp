//
//  RemoteDetailArticleLoader.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 23/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public class RemoteDetailArticleLoader: ArticleDetailLoader {
    private let url: URL
    private let client: HTTPClient
    
    public init(url: URL, client: HTTPClient) {
        self.url = url
        self.client = client
    }
    
    public enum Error: Swift.Error {
        case connectivity
        case invalidData
        case unauthorized
    }
    
    public typealias Result = ArticleDetailLoader.Result
    
    public func load(completion: @escaping (Result) -> Void) {
        client.get(from: url) { [weak self] result in
            guard self != nil else { return }
            
            switch result {
            case let .success((data, response)):
                completion(RemoteDetailArticleLoader.map(data, from: response))
                
            case let .failure(error as URLSessionHTTPClient.Error):
                switch error {
                case .unauthorized:
                    completion(.failure(Error.unauthorized))
                default:
                    completion(.failure(Error.connectivity))
                }
            default:
                completion(.failure(Error.connectivity))
            }
            
        }
    }
    
    private static func map(_ data: Data, from response: HTTPURLResponse) -> Result {
        do {
            let item = try ArticlesMapper.mapDetail(data, from: response)
            guard let itemModel = item.toModel() else {
                return .failure(Error.invalidData)
            }
            return .success(itemModel)
        } catch {
            return .failure(error)
        }
    }
}
