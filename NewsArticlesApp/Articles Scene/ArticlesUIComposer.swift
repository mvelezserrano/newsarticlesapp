//
//  ArticlesUIComposer.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

public final class ArticlesUIComposer {
    private init() {}
    
    static func articlesListComposedWith(httpClient: HTTPClient) -> (navVC: UINavigationController, vc: ArticlesListViewController) {
        let loader = RemoteArticlesLoader(url: URL(string: Configuration.APIEndpoints.articlesURL)!, client: httpClient)
        let imageLoader = RemoteImageDataLoader(client: httpClient)
        let interactor = ArticlesListInteractor(articlesLoader: MainQueueDispatchDecorator(decoratee: loader), imageDataLoader:  MainQueueDispatchDecorator(decoratee: imageLoader))
        let viewController = makeArticlesListViewController(interactor: interactor, title: ArticlesListPresenter.title)
        
        interactor.presenter = ArticlesListPresenter(
            articlesListView: viewController,
            loadingView: WeakRefVirtualProxy(viewController),
            errorView: WeakRefVirtualProxy(viewController)
        )
        
        return (viewController.navigationController!, viewController)
    }
    
    private static func makeArticlesListViewController(interactor: ArticlesListInteractor, title: String) -> ArticlesListViewController {
        let bundle = Bundle(for: ArticlesListViewController.self)
        let storyboard = UIStoryboard(name: "Articles", bundle: bundle)
        let navController = storyboard.instantiateInitialViewController() as! UINavigationController
        let controller = navController.viewControllers.first as! ArticlesListViewController
        controller.interactor = interactor
        controller.title = title
        return controller
    }
}
