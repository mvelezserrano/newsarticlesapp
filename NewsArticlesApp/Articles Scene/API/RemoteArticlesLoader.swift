//
//  RemoteArticlesLoader.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 21/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public class RemoteArticlesLoader: ArticlesLoader {
    private let url: URL
    private let client: HTTPClient
    
    public init(url: URL, client: HTTPClient) {
        self.url = url
        self.client = client
    }
    
    public enum Error: Swift.Error {
        case connectivity
        case invalidData
        case unauthorized
    }
    
    public typealias Result = ArticlesLoader.Result
    
    public func load(completion: @escaping (Result) -> Void) {
        client.get(from: url) { [weak self] result in
            guard self != nil else { return }
            
            switch result {
            case let .success((data, response)):
                completion(RemoteArticlesLoader.map(data, from: response))
                
            case let .failure(error as URLSessionHTTPClient.Error):
                switch error {
                case .unauthorized:
                    completion(.failure(Error.unauthorized))
                default:
                    completion(.failure(Error.connectivity))
                }
            default:
                completion(.failure(Error.connectivity))
            }
            
        }
    }
    
    private static func map(_ data: Data, from response: HTTPURLResponse) -> Result {
        do {
            let items = try ArticlesMapper.map(data, from: response)
            return .success(items.toSummaryModels())
        } catch {
            return .failure(error)
        }
    }
}

public extension Array where Element == ArticleDTO {
    func toSummaryModels() -> [SummaryArticle] {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-d"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        return map { SummaryArticle(id: $0.id, date: formatter.date(from: $0.date)!, summary: $0.summary, thumbnailTemplateUrl: URL(string: $0.thumbnail_template_url)!, thumbnailUrl: URL(string: $0.thumbnail_url)!, title: $0.title) }
    }
}
