//
//  Keychain.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 20/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation
import Security

public protocol KeychainWrapperProtocol {
    func set(value: String, key: String) throws
    func get(key: String) -> String?
    func delete(key: String) throws
    func deleteAll() throws
}
    
public protocol KeychainStoreProtocol {
    var service: String { get }
    typealias KeychainStoreResult = Swift.Result<Void, Error>
    func storeAuthDataFromLogin(user: String, password: String, andToken token: Token, completion: @escaping (KeychainStoreResult) -> Void)
    func storeAuthDataFromRefreshToken(token: Token, completion: @escaping (KeychainStoreResult) -> Void)
    func getAccessToken() -> String?
    func getRefreshToken() -> String?
    func userLogedIn() -> Bool
    func removeUserData(completion: @escaping (KeychainStoreResult) -> Void)
}

public struct KeychainStore: KeychainStoreProtocol, KeychainWrapperProtocol {
    public var service: String
    private var defaults = UserDefaults.standard
    
    private let passwordKey = Configuration.Keychain.passwordKey
    private let accessTokenKey = Configuration.Keychain.accessTokenKey
    private let refreshTokenKey = Configuration.Keychain.refreshTokenKey
    private let userLogedInKey = Configuration.Keychain.userLogedInKey
    
    init(service: String) {
        self.service = service
    }
    
    // MARK: KeychainStoreProtocol
    
    public func userLogedIn() -> Bool {
        return defaults.bool(forKey: userLogedInKey)
    }
    
    public func removeUserData(completion: @escaping (KeychainStoreResult) -> Void) {
        do {
            try deleteAll()
            defaults.removeObject(forKey: userLogedInKey)
            completion(.success(()))
        } catch {
            completion(.failure(Error.keychainError))
        }
    }
    
    public func getAccessToken() -> String? {
        return get(key: accessTokenKey)
    }
    
    public func getRefreshToken() -> String? {
        return get(key: refreshTokenKey)
    }
    
    public func storeAuthDataFromLogin(user: String, password: String, andToken token: Token, completion: @escaping (KeychainStoreResult) -> Void) {
        do {
            try set(value: password, key: passwordKey)
            try set(value: token.accessToken, key: accessTokenKey)
            try set(value: token.refreshToken, key: refreshTokenKey)
            defaults.set(true, forKey: userLogedInKey)
            completion(.success(()))
        } catch {
            defaults.removeObject(forKey: userLogedInKey)
            completion(.failure(Error.keychainError))
        }
    }
    
    public func storeAuthDataFromRefreshToken(token: Token, completion: @escaping (KeychainStoreResult) -> Void) {
        do {
            try set(value: token.accessToken, key: accessTokenKey)
            try set(value: token.refreshToken, key: refreshTokenKey)
            defaults.set(true, forKey: userLogedInKey)
            completion(.success(()))
        } catch {
            defaults.removeObject(forKey: userLogedInKey)
            completion(.failure(Error.keychainError))
        }
    }
    
    // MARK: Public interface
    
    public func set(value: String, key: String) throws {
        try Keychain.set(value: value.data(using: .utf8)!, key: key, service: service)
    }
    
    public func get(key: String) -> String? {
        guard let data = try? Keychain.get(key: key, service: service) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
    
    public func delete(key: String) throws {
        try Keychain.delete(key: key, service: service)
    }
    
    public func deleteAll() throws {
        try Keychain.deleteAll()
    }
    
    enum Error: Swift.Error {
        case keychainError
    }
    
    // MARK: KeychainWrapperProtocol
    
    private struct Keychain {
        static func exists(key: String, service: String) throws -> Bool {
            let status = SecItemCopyMatching([
                kSecClass: kSecClassGenericPassword,
                kSecAttrAccount: key,
                kSecAttrService: service,
                kSecReturnData: false,
                ] as NSDictionary, nil)
            if status == errSecSuccess {
                return true
            } else if status == errSecItemNotFound {
                return false
            } else {
                throw Error.keychainError
            }
        }
        
        private static func add(value: Data, key: String, service: String) throws {
            let status = SecItemAdd([
                kSecClass: kSecClassGenericPassword,
                kSecAttrAccount: key,
                kSecAttrService: service,
                // Allow background access:
                kSecAttrAccessible: kSecAttrAccessibleAfterFirstUnlock,
                kSecValueData: value,
                ] as NSDictionary, nil)
            guard status == errSecSuccess else { throw Error.keychainError }
        }
        
        private static func update(value: Data, key: String, service: String) throws {
            let status = SecItemUpdate([
                kSecClass: kSecClassGenericPassword,
                kSecAttrAccount: key,
                kSecAttrService: service,
                ] as NSDictionary, [
                    kSecValueData: value,
                    ] as NSDictionary)
            guard status == errSecSuccess else { throw Error.keychainError }
        }
        
        static func set(value: Data, key: String, service: String) throws {
            if try exists(key: key, service: service) {
                try update(value: value, key: key, service: service)
            } else {
                try add(value: value, key: key, service: service)
            }
        }
        
        static func get(key: String, service: String) throws -> Data? {
            var result: AnyObject?
            let status = SecItemCopyMatching([
                kSecClass: kSecClassGenericPassword,
                kSecAttrAccount: key,
                kSecAttrService: service,
                kSecReturnData: true,
                ] as NSDictionary, &result)
            if status == errSecSuccess {
                return result as? Data
            } else if status == errSecItemNotFound {
                return nil
            } else {
                throw Error.keychainError
            }
        }
        
        static func delete(key: String, service: String) throws {
            let status = SecItemDelete([
                kSecClass: kSecClassGenericPassword,
                kSecAttrAccount: key,
                kSecAttrService: service,
                ] as NSDictionary)
            guard status == errSecSuccess else { throw Error.keychainError }
        }
        
        static func deleteAll() throws {
            let status = SecItemDelete([
                kSecClass: kSecClassGenericPassword,
                ] as NSDictionary)
            guard status == errSecSuccess else { throw Error.keychainError }
        }
    }
}
