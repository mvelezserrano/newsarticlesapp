//
//  ArticlesListPresenter.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public protocol ArticlesListView {
    func display(_ viewModel: ArticlesListViewModel)
}

public final class ArticlesListPresenter {
    private let articlesListView: ArticlesListView
    private let loadingView: LoadingView
    private let errorView: ErrorMessageView
    
    public init(articlesListView: ArticlesListView, loadingView: LoadingView, errorView: ErrorMessageView) {
        self.articlesListView = articlesListView
        self.loadingView = loadingView
        self.errorView = errorView
    }
    
    public static var title: String {
        return NSLocalizedString("ARTICLES_LIST_VIEW_TITLE", tableName: "Articles", bundle: Bundle(for: ArticlesListPresenter.self), comment: "Title for the articles list view")
    }
    
    private var articlesListLoadError: String {
        return NSLocalizedString("ARTICLES_LIST_VIEW_CONNECTION_ERROR", tableName: "Articles", bundle: Bundle(for: ArticlesListPresenter.self), comment: "Error message displayed when we can't load articles list from the server")
    }
    
    public func didStartLoadingArticles() {
        errorView.display(.noError)
        loadingView.display(LoadingViewModel(isLoading: true))
    }
    
    public func didFinishLoadingArticles(with articles: [SummaryArticle]) {
        articlesListView.display(ArticlesListViewModel(articles:articles))
        loadingView.display(LoadingViewModel(isLoading: false))
    }
    
    public func didFinishLoadingArticles(with error: Error) {
        errorView.display(.error(message: articlesListLoadError))
        loadingView.display(LoadingViewModel(isLoading: false))
    }
}
