//
//  HTTPURLResponse+StatusCode.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 21/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

extension HTTPURLResponse {
    private static var OK_201: Int { return 201 }
    private static var OK_200: Int { return 200 }
    private static var KO_401: Int { return 401 }

    var created: Bool {
        return statusCode == HTTPURLResponse.OK_201
    }

    var isOK: Bool {
        return statusCode == HTTPURLResponse.OK_200
    }
    
    var unauthorized: Bool {
        return statusCode == HTTPURLResponse.KO_401
    }
}
