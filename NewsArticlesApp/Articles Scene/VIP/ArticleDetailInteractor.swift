//
//  ArticleDetailInteractor.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 26/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public final class ArticleDetailInteractor {
    private let articleDetailLoader: ArticleDetailLoader
    private let imageDataLoader: ImageDataLoader
    private let favoritesStore: FavoritesStore
    
    private var task: ImageDataLoaderTask?
    private var model: Article
    
    var presenter: ArticleDetailPresenter?
    
    init(summaryArticle: SummaryArticle, articleDetailLoader: ArticleDetailLoader, imageDataLoader: ImageDataLoader, favoritesStore: FavoritesStore) {
        self.articleDetailLoader = articleDetailLoader
        self.imageDataLoader = imageDataLoader
        self.favoritesStore = favoritesStore
        self.model = summaryArticle.toArticle()
    }
    
    func requestArticleDetail() {
        presenter?.didStartLoadingArticle()
        
        articleDetailLoader.load { [weak self] result in
            switch result {
            case let .success(article):
                self?.completeDataForArticle(article)
                
            case let .failure(error):
                self?.presenter?.didFinishLoadingArticle(with: error)
            }
        }
    }
    
    private func completeDataForArticle(_ article: Article) {
        model = article
        requestArticleImage()
        retrieveArticleIsFavorite(for: article) { [weak self] isFavorite in
            guard let self = self else { return }
            self.model.isFavorite = isFavorite
            self.presenter?.didFinishLoadingArticle(with: self.model)
        }
    }
    
    private func retrieveArticleIsFavorite(for model: Article, completion: @escaping (Bool) -> Void) {
        favoritesStore.retrieve { [weak self] result in
            guard self != nil else { return }
            guard case let .success(currentFavorites) = result else {
                completion(false)
                return
            }
            completion(currentFavorites.contains(model.id))
        }
    }
    
    private func requestArticleImage() {
        guard let imageURL = model.imageUrl else {
            return
        }
        task = imageDataLoader.loadImageData(from: imageURL) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(data):
                self.presenter?.didFinishLoadingData(with: data, for: self.model)
                
            case let .failure(error):
                self.presenter?.didFinishLoadingData(with: error, for: self.model)
            }
        }
    }
    
    func cancelImageRequest() {
        task?.cancel()
    }
    
    func addOrRemoveFromFavorites() {
        let newValue = !model.isFavorite
        let favoritesStoreCompletion: (Result<Void, Error>) -> () = { [weak self] storeResult in
            guard let self = self else { return }
            switch storeResult {
            case .success:
                self.model.isFavorite = newValue
                self.presenter?.didFinishLoadingArticle(with: self.model)
            default:
                break
            }
        }
        if newValue {
            favoritesStore.insert(favoriteId: model.id, completion: favoritesStoreCompletion)
        } else {
            favoritesStore.delete(favoriteId: model.id, completion: favoritesStoreCompletion)
        }
    }
}
