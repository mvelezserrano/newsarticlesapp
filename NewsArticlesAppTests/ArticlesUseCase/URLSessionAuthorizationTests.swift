//
//  URLSessionAuthorizationTests.swift
//  NewsArticlesAppTests
//
//  Created by Miguel Angel Vélez Serrano on 22/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import XCTest

class URLSessionAuthorizationTests: XCTestCase {
    
    override func tearDown() {
        super.tearDown()
        
        URLProtocolStub.removeStubs()
    }
    
    func test_getFromURL_failsUsingExpiredRefreshToken() {
        let (sut, _) = makeSUT()
        
        expect(sut, toCompleteWith: failure(.unauthorized), when: {
            completeWithStubbedResponses([.unauthorized, .unauthorized])
        })
    }
    
    func test_getFromURL_succeedsUsingValidAccessTokenAfterInvalidTokenRefreshed() {
        let (sut, _) = makeSUT()
        
        let item1 = makeSummaryArticle(id: UUID().hashValue, date: Date.init(), summary: "aSummary", thumbnailTemplateUrl: URL(string: "http://a-thumbnail-template-url.com")!, thumbnailUrl: URL(string: "http://a-thumbnail-url.com")!, title: "aTitle")
        
        let item2 = makeSummaryArticle(id: UUID().hashValue, date: Date.init(), summary: "anotherSummary", thumbnailTemplateUrl: URL(string: "http://another-thumbnail-template-url.com")!, thumbnailUrl: URL(string: "http://another-thumbnail-url.com")!, title: "anotherTitle")
        let items = [item1.model, item2.model]
        
        let jsonData = makeSummaryArticlesJSON([item1.json, item2.json])
        
        expect(sut, toCompleteWith: .success(items), when: {
            completeWithStubbedResponses([.unauthorized, .created(makeTokenJSON()), .success(jsonData)])
        })
    }
    
    // MARK: Helper methods
    
    private func makeSUT(url: URL = URL(string: "https://a-url.com")!, file: StaticString = #file, line: UInt = #line) -> (sut: RemoteArticlesLoader, client: URLSessionHTTPClient) {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [URLProtocolStub.self]
        let session = URLSession(configuration: configuration)
        let keychainStore = KeychainStoreSpy(service: XCTestCase.testService)
        let client = URLSessionHTTPClient(session:session, keychainStore: keychainStore)
        let sut = RemoteArticlesLoader(url: url, client: client)
        trackForMemoryLeaks(sut, file: file, line: line)
        trackForMemoryLeaks(client, file: file, line: line)
        return (sut, client)
    }
    
    private func expect(_ sut: RemoteArticlesLoader, toCompleteWith expectedResult: RemoteArticlesLoader.Result, when action: () -> Void, file: StaticString = #file, line: UInt = #line) {
        let exp = expectation(description: "Wait for load completion")
        
        sut.load { receivedResult in
            switch (receivedResult, expectedResult) {
            case let (.success(receivedItems), .success(expectedItems)):
                XCTAssertEqual(receivedItems, expectedItems, file: file, line: line)

            case let (.failure(receivedError as RemoteArticlesLoader.Error), .failure(expectedError as RemoteArticlesLoader.Error)):
                XCTAssertEqual(receivedError, expectedError, file: file, line: line)

            default:
                XCTFail("Expected result \(expectedResult) got \(receivedResult) instead", file: file, line: line)
            }
            
            exp.fulfill()
        }
        
        action()
        wait(for: [exp], timeout: 1.0)
    }
    
    private func failure(_ error: RemoteArticlesLoader.Error) -> RemoteArticlesLoader.Result {
        return .failure(error)
    }
    
    private enum StubbedResponse {
        case unauthorized
        case created(Data?)
        case success(Data?)
        
        var values: (data: Data?, response: HTTPURLResponse?, error: Error?) {
            switch self {
            case .unauthorized:
                return (data: anyData(), response: httpUrlResponseWith(code: 401), error: nil)
            case let .created(data):
                return (data: data, response: httpUrlResponseWith(code: 201), error: nil)
            case let .success(data):
                return (data: data, response: httpUrlResponseWith(code: 200), error: nil)
            }
        }
    }
    
    private func completeWithStubbedResponses(_ stubbedResponses: [StubbedResponse]) {
        _ = stubbedResponses.map { stubbedResponse in
            return URLProtocolStub.stub(data: stubbedResponse.values.data, response: stubbedResponse.values.response, error: stubbedResponse.values.error)
        }
    }
}
