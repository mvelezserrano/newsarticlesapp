//
//  AuthService.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 19/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public final class AuthService: AuthServiceProtocol {
    private static let loginURL = URL(string: Configuration.APIEndpoints.loginURL)!
    private let client: HTTPClient
    private let keychainStore: KeychainStoreProtocol
    
    public enum Error: Swift.Error {
        case connectivity
        case invalidData
        case unauthorized
    }
    
    private enum AuthType {
        case login(user: String, password: String)
        case refreshToken(refreshToken: String?)
    }
    
    public init(client: HTTPClient, keychainStore: KeychainStoreProtocol) {
        self.client = client
        self.keychainStore = keychainStore
    }
    
    public typealias Result = AuthServiceProtocol.Result
    
    public func loginWith(user: String, andPassword password: String, completion: @escaping (Result) -> Void) {
        executeAuthRequestFor(.login(user: user, password: password), completion: completion)
    }
    
    public func logout(completion: @escaping () -> Void) {
        keychainStore.removeUserData { _ in
            completion()
        }
    }
    
    public func refreshToken(completion: @escaping (Result) -> Void) {
        executeAuthRequestFor(.refreshToken(refreshToken: keychainStore.getRefreshToken()), completion: completion)
    }
    
    private func executeAuthRequestFor(_ authType: AuthType, completion: @escaping (Result) -> Void) {
        var urlRequest = URLRequest(url: AuthService.loginURL)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = AuthService.jsonBodyForAuthType(authType)
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        client.post(with: urlRequest) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case let .success((data, response)):
                self.manageAuthServiceResponse(data, response: response, forAuthType: authType, completion: completion)
            case .failure:
                completion(.failure(Error.connectivity))
            }
        }
    }
    
    private func manageAuthServiceResponse(_ data: Data, response: HTTPURLResponse, forAuthType authType: AuthType, completion: @escaping (Result) -> Void) {
        let mapResult = AuthService.map(data, from: response)
        guard case let .success(token) = mapResult else {
            completion(mapResult)
            return
        }
        
        let storeAuthCompletion: (KeychainStoreProtocol.KeychainStoreResult) -> () = { storeResult in
            switch storeResult {
            case .success:
                completion(mapResult)
            default:
                completion(.failure(Error.invalidData))
            }
        }
        
        switch authType {
        case let .login(user, password):
            keychainStore.storeAuthDataFromLogin(user: user, password: password, andToken: token, completion: storeAuthCompletion)
        default:
            keychainStore.storeAuthDataFromRefreshToken(token: token, completion: storeAuthCompletion)
        }
    }
    
    public static func map(_ data: Data, from response: HTTPURLResponse) -> Result {
        do {
            let tokenDTO = try TokenMapper.map(data, from: response)
            return .success(tokenDTO.toModel())
        } catch {
            return .failure(error)
        }
    }
    
    private static func jsonBodyForAuthType(_ authType: AuthType) -> Data? {
        var json: [String: Any] = [:]
        switch authType {
        case let .login(user: user, password: password):
            json = [
                "username": user,
                "password": password,
                "grant_type": "password"
            ]
        case let .refreshToken(refreshToken: refreshToken) where refreshToken != nil:
            json = [
                "refresh_token": refreshToken!,
                "grant_type": "refresh_token"
            ]
        default:
            return nil
        }
        return try? JSONSerialization.data(withJSONObject: json)
    }
    
    public static func refreshTokenRequestUsingRefreshToken(_ refreshToken: String?) -> URLRequest {
        var urlRequest = URLRequest(url: loginURL)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = jsonBodyForAuthType(.refreshToken(refreshToken: refreshToken))
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        return urlRequest
    }
}
