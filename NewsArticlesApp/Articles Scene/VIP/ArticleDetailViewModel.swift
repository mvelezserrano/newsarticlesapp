//
//  ArticleDetailViewModel.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 26/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

public struct ArticleDetailViewModel {
    public let date: String
    public let title: String
    public let image: UIImage?
    public let content: String?
    public let sourceUrl: URL?
    public let isFavorite: Bool
}
