//
//  ArticlesLoader.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 20/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public protocol ArticlesLoader {
    typealias Result = Swift.Result<[SummaryArticle], Error>
    
    func load(completion: @escaping (Result) -> Void)
}
