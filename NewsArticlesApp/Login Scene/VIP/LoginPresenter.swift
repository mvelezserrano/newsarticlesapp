//
//  LoginPresenter.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public protocol LoginView {
    func display(_ viewModel: LoginViewModel)
}

public final class LoginPresenter {
    private let loginView: LoginView
    private let loadingView: LoadingView
    private let errorView: ErrorMessageView
    
    public init(loginView: LoginView, loadingView: LoadingView, errorView: ErrorMessageView) {
        self.loginView = loginView
        self.loadingView = loadingView
        self.errorView = errorView
    }
    
    public enum Error: Swift.Error {
        case serverError
        case unauthorized
    }
    
    private var loginServerError: String {
        return NSLocalizedString("LOGIN_VIEW_LOGIN_SERVER_ERROR", tableName: "Login", bundle: Bundle(for: LoginPresenter.self), comment: "Error message displayed when we can't get login response from the server or response data invalid")
    }
    
    private var loginFailedError: String {
        return NSLocalizedString("LOGIN_VIEW_LOGIN_INVALID_ERROR", tableName: "Login", bundle: Bundle(for: LoginPresenter.self), comment: "Error message displayed when login credentials incorrect")
    }
    
    private var loginSucceed: String {
        return NSLocalizedString("LOGIN_VIEW_LOGIN_SUCCESS", tableName: "Login", bundle: Bundle(for: LoginPresenter.self), comment: "Message displayed when login succeed")
    }
    
    public func didStartLoginRequest() {
        errorView.display(.noError)
        loadingView.display(LoadingViewModel(isLoading: true))
    }
    
    public func didFinishLoginRequestWithSuccess() {
        loginView.display(LoginViewModel(loginMessage: loginSucceed))
        loadingView.display(LoadingViewModel(isLoading: false))
    }
    
    public func didFinishLoginRequest(with error: Error) {
        switch error {
        case .serverError:
            errorView.display(.error(message: loginServerError))
        case .unauthorized:
            errorView.display(.error(message: loginFailedError))
        }
        loadingView.display(LoadingViewModel(isLoading: false))
    }
}
