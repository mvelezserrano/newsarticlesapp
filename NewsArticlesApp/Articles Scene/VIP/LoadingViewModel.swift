//
//  ArticlesListLoadingViewModel.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public struct LoadingViewModel {
    public let isLoading: Bool
}
