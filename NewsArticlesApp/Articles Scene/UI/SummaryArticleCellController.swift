//
//  ArticleCellController.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

public final class ArticleCellController: SummaryArticleView {
    private let interactor: ImageDataLoaderInteractor
    private var cell: SummaryArticleCell?
    
    public init(interactor: ImageDataLoaderInteractor) {
        self.interactor = interactor
    }
    
    func view(in tableView: UITableView) -> UITableViewCell {
        cell = tableView.dequeueReusableCell()
        interactor.requestImage()
        return cell!
    }
    
    func preload() {
        interactor.requestImage()
    }
    
    func cancelLoad() {
        releaseCellForReuse()
        interactor.cancelImageRequest()
    }
    
    public func display(_ viewModel: SummaryArticleViewModel) {
        cell?.dateLabel.text = viewModel.date
        cell?.titleLabel.text = viewModel.title
        cell?.summaryLabel.text = viewModel.summary
        cell?.thumbnailImageView.setImageAnimated(viewModel.image)
    }
    
    private func releaseCellForReuse() {
        cell = nil
    }
}
