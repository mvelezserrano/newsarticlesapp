//
//  Configuration.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 20/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

final class Configuration {
    class Appearance {
        static let mainColor = UIColor(red: 25/255, green: 28/255, blue: 59/255, alpha: 1.0)
    }
    class Keychain {
        static let service = "NewsArticlesAppKeychainService"
        static let passwordKey = "NewsArticlesKeychainPasswordKey"
        static let accessTokenKey = "NewsArticlesKeychainAccessTokenKey"
        static let refreshTokenKey = "NewsArticlesKeychainRefreshTokenKey"
        static let userLogedInKey = "NewsArticlesUserLogedInKey"
    }
    
    class APIEndpoints {
        static let loginURL = "http://amock.io/api/Mixi/auth/token"
        static let articlesURL = "http://amock.io/api/Mixi/v1/articles"
    }
}
