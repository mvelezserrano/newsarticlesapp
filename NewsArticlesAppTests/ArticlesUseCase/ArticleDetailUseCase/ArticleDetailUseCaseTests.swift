//
//  ArticleDetailUseCaseTests.swift
//  NewsArticlesAppTests
//
//  Created by Miguel Angel Vélez Serrano on 23/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import XCTest

class ArticleDetailUseCaseTests: XCTestCase {
    func test_load_deliversErrorOnClientError() {
        let (sut, client) = makeSUT()
        
        expect(sut, toCompleteWith: failure(.connectivity), when: {
            let clientError = NSError(domain: "Test", code: 0)
            client.complete(with: clientError)
        })
    }
    
    func test_load_deliversErrorOnNon200HTTPResponse() {
        let (sut, client) = makeSUT()
        
        let item = makeItem()
        let samples = [199, 201, 300, 400, 500]
        
        samples.enumerated().forEach { index, code in
            expect(sut, toCompleteWith: failure(.invalidData), when: {
                let json = makeArticleJSON(item.json)
                client.complete(withStatusCode: code, data: json, at: index)
            })
        }
    }
    
    func test_load_deliversErrorOn200HTTPResponseWithInvalidJSON() {
        let (sut, client) = makeSUT()
        
        expect(sut, toCompleteWith: failure(.invalidData), when: {
            let invalidJSON = Data("invalid json".utf8)
            client.complete(withStatusCode: 200, data: invalidJSON)
        })
    }
    
    func test_load_deliversItemDetailOn200HTTPResponseWithJSONItemDetail() {
        let (sut, client) = makeSUT()
        
        let item = makeItem()
        
        expect(sut, toCompleteWith: .success(item.model), when: {
            let json = makeArticleJSON(item.json)
            client.complete(withStatusCode: 200, data: json)
        })
    }
    
    func test_load_doesNotDeliverResultAfterSUTInstanceHasBeenDeallocated() {
        let url = URL(string: "http://any-url.com")!
        let client = HTTPClientSpy()
        var sut: RemoteDetailArticleLoader? = RemoteDetailArticleLoader(url: url, client: client)
        
        var capturedResults = [RemoteDetailArticleLoader.Result]()
        sut?.load { capturedResults.append($0) }

        sut = nil
        client.complete(withStatusCode: 200, data: makeArticleJSON(makeItem().json))
        
        XCTAssertTrue(capturedResults.isEmpty)
    }
    
    // MARK: - Helpers
    
    private func makeSUT(url: URL = URL(string: "https://a-url.com")!, file: StaticString = #file, line: UInt = #line) -> (sut: RemoteDetailArticleLoader, client: HTTPClientSpy) {
        let client = HTTPClientSpy()
        let sut = RemoteDetailArticleLoader(url: url, client: client)
        trackForMemoryLeaks(sut, file: file, line: line)
        trackForMemoryLeaks(client, file: file, line: line)
        return (sut, client)
    }
    
    private func failure(_ error: RemoteDetailArticleLoader.Error) -> RemoteDetailArticleLoader.Result {
        return .failure(error)
    }
    
    private func expect(_ sut: RemoteDetailArticleLoader, toCompleteWith expectedResult: RemoteDetailArticleLoader.Result, when action: () -> Void, file: StaticString = #file, line: UInt = #line) {
        let exp = expectation(description: "Wait for load completion")
        
        sut.load { receivedResult in
            switch (receivedResult, expectedResult) {
            case let (.success(receivedItems), .success(expectedItems)):
                XCTAssertEqual(receivedItems, expectedItems, file: file, line: line)

            case let (.failure(receivedError as RemoteDetailArticleLoader.Error), .failure(expectedError as RemoteDetailArticleLoader.Error)):
                XCTAssertEqual(receivedError, expectedError, file: file, line: line)

            default:
                XCTFail("Expected result \(expectedResult) got \(receivedResult) instead", file: file, line: line)
            }
            
            exp.fulfill()
        }
        
        action()
        wait(for: [exp], timeout: 1.0)
    }
    
    private func makeItem() -> (model: Article, json: [String: Any]) {
        return makeArticle(id: UUID().hashValue, date: Date.init(), summary: "aSummary", thumbnailTemplateUrl: URL(string: "http://a-thumbnail-template-url.com")!, thumbnailUrl: URL(string: "http://a-thumbnail-url.com")!, title: "aTitle", content: "aContent", imageURL: URL(string: "http://a-image-url.com")!, sourceURL: URL(string: "http://a-source-url.com")!)
    }
}


