//
//  SharedTestHelpers.swift
//  NewsArticlesAppTests
//
//  Created by Miguel Angel Vélez Serrano on 23/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

func makeSummaryArticlesJSON(_ items: [[String: Any]]) -> Data {
    return try! JSONSerialization.data(withJSONObject: items)
}

func makeArticleJSON(_ item: [String: Any]) -> Data {
    return try! JSONSerialization.data(withJSONObject: item)
}

func makeTokenJSON() -> Data {
    let json = ["access_token" : "abcd", "refresh_token": "efgh"]
    return try! JSONSerialization.data(withJSONObject: json)
}

func makeSummaryArticle(id: Int, date: Date, summary: String, thumbnailTemplateUrl: URL, thumbnailUrl: URL, title: String) -> (model: SummaryArticle, json: [String: Any]) {
    
    let item = SummaryArticle(id: id, date: date, summary: summary, thumbnailTemplateUrl: thumbnailTemplateUrl, thumbnailUrl: thumbnailUrl, title: title)
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "YYYY-MM-d"
    
    let json = [
        "id": id,
        "date": dateFormatter.string(from: date),
        "summary": summary,
        "thumbnail_template_url": thumbnailTemplateUrl.absoluteString,
        "thumbnail_url": thumbnailUrl.absoluteString,
        "title": title
    ].compactMapValues { $0 }
    
    return (item, json)
}

func makeArticle(id: Int, date: Date, summary: String, thumbnailTemplateUrl: URL, thumbnailUrl: URL, title: String, content: String, imageURL: URL, sourceURL: URL) -> (model: Article, json: [String: Any]) {
    
    let item = Article(id: id, date: date, summary: summary, thumbnailTemplateUrl: thumbnailTemplateUrl, thumbnailUrl: thumbnailUrl, title: title, content: content, imageUrl: imageURL, sourceUrl: sourceURL, isFavorite: false)
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "YYYY-MM-d"
    
    let json = [
        "id": id,
        "date": dateFormatter.string(from: date),
        "summary": summary,
        "thumbnail_template_url": thumbnailTemplateUrl.absoluteString,
        "thumbnail_url": thumbnailUrl.absoluteString,
        "title": title,
        "content": content,
        "image_url": imageURL.absoluteString,
        "source_url": sourceURL.absoluteString
    ].compactMapValues { $0 }
    
    return (item, json)
}

func anyNSError() -> NSError {
    return NSError(domain: "any error", code: 0)
}

func anyURL() -> URL {
    return URL(string: "http://any-url.com")!
}

func anyData() -> Data {
    return Data("any data".utf8)
}

func httpUrlResponseWith(code: Int) -> HTTPURLResponse {
    return HTTPURLResponse(url: URL(string: "http://any-url.com")!, statusCode: code, httpVersion: nil, headerFields: nil)!
}

func nonHTTPURLResponse() -> URLResponse {
    return URLResponse(url: anyURL(), mimeType: nil, expectedContentLength: 0, textEncodingName: nil)
}
