//
//  ArticleDetailLoader.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 23/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public protocol ArticleDetailLoader {
    typealias Result = Swift.Result<Article, Error>
    
    func load(completion: @escaping (Result) -> Void)
}
