//
//  FavoritesStore.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 24/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public typealias SavedFavorites = [Int]

public protocol FavoritesStore {
    typealias RetrievalResult = Result<SavedFavorites, Error>
    typealias RetrievalCompletion = (RetrievalResult) -> Void
    
    typealias InsertionResult = Result<Void, Error>
    typealias InsertionCompletion = (InsertionResult) -> Void
    
    typealias DeletionResult = Result<Void, Error>
    typealias DeletionCompletion = (DeletionResult) -> Void
    
    typealias IsFavoriteResult = Result<Bool, Error>
    typealias IsFavoriteCompletion = (IsFavoriteResult) -> Void
    
    func retrieve(completion: @escaping RetrievalCompletion)
    func insert(favoriteId: Int, completion: @escaping InsertionCompletion)
    func delete(favoriteId: Int, completion: @escaping DeletionCompletion)
    func isFavorite(favoriteId: Int, completion: @escaping IsFavoriteCompletion)
}
