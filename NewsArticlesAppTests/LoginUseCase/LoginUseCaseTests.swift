//
//  LoginUseCaseTests.swift
//  LoginUseCaseTests
//
//  Created by Miguel Angel Vélez Serrano on 19/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import XCTest

class LoginUseCaseTests: XCTestCase {
    
    func test_init_doesNotRequestsLogin() {
        let (_, client) = makeSUT()
        
        XCTAssertTrue(client.requestedURLs.isEmpty)
    }
    
    func test_login_requestsLoginWithCorrectlyConfiguredURLRequest() {
        let url = URL(string: Configuration.APIEndpoints.loginURL)!
        let (sut, client) = makeSUT()
        let user = "user"
        let password = "password"
        
        sut.loginWith(user: user, andPassword: password) {_ in }
        let urlRequest = client.urlRequests.first!
        
        XCTAssertEqual(client.requestedURLs, [url])
        XCTAssertEqual(urlRequest.httpMethod, "POST")
        guard let urlRequestBodyJSON = try? JSONSerialization.jsonObject(with: urlRequest.httpBody!, options: []) as? [String: String] else {
            XCTFail("Unable to parse urlRequest.httpBody to JSON")
            return
        }
        XCTAssertEqual(urlRequestBodyJSON["username"], user)
        XCTAssertEqual(urlRequestBodyJSON["password"], password)
        XCTAssertEqual(urlRequestBodyJSON["grant_type"], "password")
        XCTAssertEqual(urlRequest.value(forHTTPHeaderField: "Content-Type"), "application/json")
        
    }
    
    func test_login_deliversErrorOnClientError() {
        let (sut, client) = makeSUT()
        
        expect(sut, toCompleteWith: failure(.connectivity), when: {
            let clientError = NSError(domain: "Test", code: 0)
            client.complete(with: clientError)
        })
    }
    
    func test_login_deliversErrorOnNon201HTTPResponse() {
        let (sut, client) = makeSUT()
        
        let samples = [199, 200, 300, 400, 500]
        
        samples.enumerated().forEach { index, code in
            expect(sut, toCompleteWith: failure(.unauthorized), when: {
                let json = makeDefaultLoginResponseJSON()
                client.complete(withStatusCode: code, data: json, at: index)
            })
        }
    }
    
    func test_login_deliversErrorOn201HTTPResponseWithInvalidJSON() {
        let (sut, client) = makeSUT()
        
        expect(sut, toCompleteWith: failure(.invalidData), when: {
            let invalidJSON = Data("invalid json response".utf8)
            client.complete(withStatusCode: 201, data: invalidJSON)
        })
    }
    
    func test_login_deliversTokenOn201HTTPResponseWithJSONToken() {
        let (sut, client) = makeSUT()
        
        expect(sut, toCompleteWith: .success(makeToken()), when: {
            let json = makeDefaultLoginResponseJSON()
            client.complete(withStatusCode: 201, data: json)
        })
    }
    
    func test_login_doesNotDeliverResultAfterSUTInstanceHasBeenDeallocated() {
        let client = HTTPClientSpy()
        var sut: AuthService? = AuthService(client: client, keychainStore: KeychainStoreSpy(service: XCTestCase.testService))
        
        var capturedResults = [AuthService.Result]()
        sut?.loginWith(user: "aUser", andPassword: "aPassword") { capturedResults.append($0) }

        sut = nil
        client.complete(withStatusCode: 201, data: makeDefaultLoginResponseJSON())
        
        XCTAssertTrue(capturedResults.isEmpty)
    }
    
    func test_login_loginSuccessfulShouldStoreAuthDataIntoKeychain() {
        let client = HTTPClientSpy()
        let keychainStore = KeychainStoreSpy(service: XCTestCase.testService)
        let sut = AuthService(client: client, keychainStore: keychainStore)
        let token = makeToken()
        
        sut.loginWith(user: "aUser", andPassword: "aPassword") { _ in }
        client.complete(withStatusCode: 201, data: makeDefaultLoginResponseJSON())
        
        XCTAssertEqual(keychainStore.receivedMessages, [.set("aPassword"), .set(token.accessToken), .set(token.refreshToken)])
    }
    
    // MARK: Helpers
    
    private func makeSUT(file: StaticString = #file, line: UInt = #line) -> (sut: AuthService, client: HTTPClientSpy) {
        let client = HTTPClientSpy()
        let keychainStore = KeychainStoreSpy(service: XCTestCase.testService)
        let sut = AuthService(client: client, keychainStore: keychainStore)
        trackForMemoryLeaks(sut, file: file, line: line)
        trackForMemoryLeaks(client, file: file, line: line)
        return (sut, client)
    }
    
    private func makeSUT(file: StaticString = #file, line: UInt = #line) -> (sut: AuthService, client: HTTPClientSpy, keychainStore: KeychainStoreProtocol) {
        let client = HTTPClientSpy()
        let keychainStore = KeychainStoreSpy(service: XCTestCase.testService)
        let sut = AuthService(client: client, keychainStore: keychainStore)
        trackForMemoryLeaks(sut, file: file, line: line)
        trackForMemoryLeaks(client, file: file, line: line)
        return (sut, client, keychainStore)
    }
    
    private func jsonBodyWith(user: String, andPassword password: String) -> Data {
        let json = [
            "username": user,
            "password": password,
            "grant_type": "password"
        ]
        return try! JSONSerialization.data(withJSONObject: json)
    }
    
    private func failure(_ error: AuthService.Error) -> AuthService.Result {
        return .failure(error)
    }
    
    private func makeToken() -> Token {
        return Token(accessToken: "abcd", refreshToken: "efgh")
    }
    
    private func makeDefaultLoginResponseJSON() -> Data {
        let json = ["access_token" : "abcd", "refresh_token": "efgh"]
        return try! JSONSerialization.data(withJSONObject: json)
    }
    
    private func expect(_ sut: AuthService, withUser user: String = "aUser", andPassword password: String = "aPassword", toCompleteWith expectedResult: AuthService.Result, when action: () -> Void, file: StaticString = #file, line: UInt = #line) {
        let exp = expectation(description: "Wait for login completion")
        
        sut.loginWith(user: user, andPassword: password) { receivedResult in
            switch (receivedResult, expectedResult) {
            case let (.success(receivedToken), .success(expectedToken)):
                XCTAssertEqual(receivedToken, expectedToken, file: file, line: line)
                
            case let (.failure(receivedError as AuthService.Error), .failure(expectedError as AuthService.Error)):
                XCTAssertEqual(receivedError, expectedError, file: file, line: line)
                
            default:
                XCTFail("Expected result \(expectedResult) got \(receivedResult) instead", file: file, line: line)
            }
            
            exp.fulfill()
        }
        
        action()
        
        wait(for: [exp], timeout: 1.0)
    }
}
