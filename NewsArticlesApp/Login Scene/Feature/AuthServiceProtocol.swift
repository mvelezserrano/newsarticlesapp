//
//  AuthServiceProtocol.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 19/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public protocol AuthServiceProtocol {
    typealias Result = Swift.Result<Token, Error>
    
    func loginWith(user: String, andPassword password: String, completion: @escaping (Result) -> Void)
    func logout(completion: @escaping () -> Void)
    func refreshToken(completion: @escaping (Result) -> Void)
}
