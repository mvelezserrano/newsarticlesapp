//
//  MainQueueDispatchDecorator.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

final class MainQueueDispatchDecorator<T> {
    private let decoratee: T

    init(decoratee: T) {
        self.decoratee = decoratee
    }
    
    func dispatch(completion: @escaping () -> Void) {
        guard Thread.isMainThread else {
            return DispatchQueue.main.async { completion() }
        }
        
        completion()
    }
}

extension MainQueueDispatchDecorator: AuthServiceProtocol where T == AuthServiceProtocol {
    func loginWith(user: String, andPassword password: String, completion: @escaping (AuthServiceProtocol.Result) -> Void) {
        decoratee.loginWith(user: user, andPassword: password) { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
    
    func logout(completion: @escaping () -> Void) {
        decoratee.logout { [weak self] in
            self?.dispatch { completion() }
        }
    }
    
    func refreshToken(completion: @escaping (AuthServiceProtocol.Result) -> Void) {
        decoratee.refreshToken { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
}



extension MainQueueDispatchDecorator: ArticlesLoader where T == ArticlesLoader {
    func load(completion: @escaping (ArticlesLoader.Result) -> Void) {
        decoratee.load { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
}

extension MainQueueDispatchDecorator: ArticleDetailLoader where T == ArticleDetailLoader {
    func load(completion: @escaping (ArticleDetailLoader.Result) -> Void) {
        decoratee.load { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
}

extension MainQueueDispatchDecorator: ImageDataLoader where T == ImageDataLoader {
    func loadImageData(from url: URL, completion: @escaping (ImageDataLoader.Result) -> Void) -> ImageDataLoaderTask {
        return decoratee.loadImageData(from: url) { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
}

extension MainQueueDispatchDecorator: FavoritesStore where T == FavoritesStore {
    func retrieve(completion: @escaping RetrievalCompletion) {
        return decoratee.retrieve { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
    
    func insert(favoriteId: Int, completion: @escaping InsertionCompletion) {
        return decoratee.insert(favoriteId: favoriteId) { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
    
    func delete(favoriteId: Int, completion: @escaping DeletionCompletion) {
        return decoratee.delete(favoriteId: favoriteId) { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
    
    func isFavorite(favoriteId: Int, completion: @escaping IsFavoriteCompletion) {
        return decoratee.isFavorite(favoriteId: favoriteId) { [weak self] result in
            self?.dispatch { completion(result) }
        }
    }
}
