//
//  URLProtocolStub.swift
//  NewsArticlesAppTests
//
//  Created by Miguel Angel Vélez Serrano on 22/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

class URLProtocolStub: URLProtocol {
    private struct Stub {
        let data: Data?
        let response: URLResponse?
        let error: Error?
        let requestObserver: ((URLRequest) -> Void)?
    }
    
    private static var _stubs: [Stub]? = []
    private static var stubs: [Stub]? {
        get { return queue.sync { _stubs } }
        set { queue.sync { _stubs = newValue } }
    }
    
    private static let queue = DispatchQueue(label: "URLProtocolStub.queue")
    
    static func stub(data: Data?, response: URLResponse?, error: Error?) {
        var currentStubs = stubs
        currentStubs?.append(Stub(data: data, response: response, error: error, requestObserver: nil))
        stubs = currentStubs
    }
    
    static func observeRequests(observer: @escaping (URLRequest) -> Void) {
        var currentStubs = stubs
        currentStubs?.append(Stub(data: nil, response: nil, error: nil, requestObserver: observer))
        stubs = currentStubs
    }
    
    static func removeStubs() {
        stubs?.removeAll()
    }
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func startLoading() {
        guard let stubs = URLProtocolStub.stubs, !stubs.isEmpty, let stub = URLProtocolStub.stubs?.removeFirst() else { return }
        
        if let data = stub.data {
            client?.urlProtocol(self, didLoad: data)
        }
        
        if let response = stub.response {
            client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
        }
        
        if let error = stub.error {
            client?.urlProtocol(self, didFailWithError: error)
        } else {
            client?.urlProtocolDidFinishLoading(self)
        }
        
        stub.requestObserver?(request)
    }
    
    override func stopLoading() {}
}
