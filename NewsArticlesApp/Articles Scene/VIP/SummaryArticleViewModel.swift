//
//  ArticleCellViewModel.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

public struct SummaryArticleViewModel {
    public let date: String
    public let title: String
    public let summary: String
    public let image: UIImage?
    public let isLoading: Bool
}
