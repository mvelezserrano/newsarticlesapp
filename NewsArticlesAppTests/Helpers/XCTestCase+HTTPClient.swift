//
//  XCTestCase+HTTPClient.swift
//  NewsArticlesAppTests
//
//  Created by Miguel Angel Vélez Serrano on 23/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import XCTest

extension XCTestCase {
    func ephemeralClient(file: StaticString = #file, line: UInt = #line) -> HTTPClient {
        let client = URLSessionHTTPClient(session: URLSession(configuration: .ephemeral), keychainStore: KeychainStore(service: XCTestCase.testService))
        trackForMemoryLeaks(client, file: file, line: line)
        return client
    }
}
