//
//  Article.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 20/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public struct SummaryArticle: Hashable {
    let id: Int
    let date: Date
    let summary: String
    let thumbnailTemplateUrl: URL
    let thumbnailUrl: URL
    let title: String
    
    func toArticle() -> Article {
        return Article(id: id, date: date, summary: summary, thumbnailTemplateUrl: thumbnailTemplateUrl, thumbnailUrl: thumbnailUrl, title: title)
    }
}

extension SummaryArticle: Equatable {
    public static func == (lhs: SummaryArticle, rhs: SummaryArticle) -> Bool {
        let lhsDateComponents = Calendar.current.dateComponents([.year, .month, .day], from: lhs.date)
        let rhsDateComponents = Calendar.current.dateComponents([.year, .month, .day], from: rhs.date)
        return
            lhs.id == rhs.id &&
            lhs.summary == rhs.summary &&
            lhs.thumbnailTemplateUrl == rhs.thumbnailTemplateUrl &&
            lhs.thumbnailUrl == rhs.thumbnailUrl &&
            lhs.title == rhs.title &&
            lhsDateComponents == rhsDateComponents
    }
}

public struct Article: Hashable {
    let id: Int
    let date: Date
    let summary: String
    let thumbnailTemplateUrl: URL
    let thumbnailUrl: URL
    let title: String
    
    let content: String?
    let imageUrl: URL?
    let sourceUrl: URL?
    var isFavorite: Bool
    
    internal init(id: Int, date: Date, summary: String, thumbnailTemplateUrl: URL, thumbnailUrl: URL, title: String) {
        self.id = id
        self.date = date
        self.summary = summary
        self.thumbnailTemplateUrl = thumbnailTemplateUrl
        self.thumbnailUrl = thumbnailUrl
        self.title = title
        self.content = nil
        self.imageUrl = nil
        self.sourceUrl = nil
        self.isFavorite = false
    }
    
    internal init(id: Int, date: Date, summary: String, thumbnailTemplateUrl: URL, thumbnailUrl: URL, title: String, content: String?, imageUrl: URL?, sourceUrl: URL?, isFavorite: Bool) {
        self.id = id
        self.date = date
        self.summary = summary
        self.thumbnailTemplateUrl = thumbnailTemplateUrl
        self.thumbnailUrl = thumbnailUrl
        self.title = title
        self.content = content
        self.imageUrl = imageUrl
        self.sourceUrl = sourceUrl
        self.isFavorite = isFavorite
    }
}

extension Article: Equatable {
    public static func == (lhs: Article, rhs: Article) -> Bool {
        let lhsDateComponents = Calendar.current.dateComponents([.year, .month, .day], from: lhs.date)
        let rhsDateComponents = Calendar.current.dateComponents([.year, .month, .day], from: rhs.date)
        return
            lhs.id == rhs.id &&
            lhs.summary == rhs.summary &&
            lhs.thumbnailTemplateUrl == rhs.thumbnailTemplateUrl &&
            lhs.thumbnailUrl == rhs.thumbnailUrl &&
            lhs.title == rhs.title &&
            lhs.content == rhs.content &&
            lhs.imageUrl == rhs.imageUrl &&
            lhs.sourceUrl == rhs.sourceUrl &&
            lhs.isFavorite == rhs.isFavorite &&
            lhsDateComponents == rhsDateComponents
    }
}
