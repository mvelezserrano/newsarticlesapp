//
//  ArticlesLoaderPresentationAdapter.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public final class ArticlesListInteractor {
    private let articlesLoader: ArticlesLoader
    private let imageDataLoader: ImageDataLoader
    
    var presenter: ArticlesListPresenter?
    private var model: [SummaryArticle] = []
    
    init(articlesLoader: ArticlesLoader, imageDataLoader: ImageDataLoader) {
        self.articlesLoader = articlesLoader
        self.imageDataLoader = imageDataLoader
    }
    
    func requestArticlesList() {
        presenter?.didStartLoadingArticles()
        
        articlesLoader.load { [weak self] result in
            switch result {
            case let .success(articles):
                self?.model = articles
                self?.presenter?.didFinishLoadingArticles(with: articles)
            case let .failure(error):
                self?.presenter?.didFinishLoadingArticles(with: error)
            }
        }
    }
    
    func articleAtIndex(_ index: Int) -> SummaryArticle? {
        guard index <= model.count else {
            return nil
        }
        return model[index]
    }
    
    func summaryArticleInteractor(for model: SummaryArticle) -> ImageDataLoaderInteractor {
        return ImageDataLoaderInteractor(model: model, imageDataLoader: imageDataLoader)
    }
}
