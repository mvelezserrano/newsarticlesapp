//
//  ArticleDetailViewController.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 26/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

protocol ArticleDetailRoutingDelegate: class {
    func didExitArticle(inVC vc: ArticleDetailViewController)
}

final class ArticleDetailViewController: UIViewController {
    
    @IBOutlet private weak var headerImageView: UIImageView!
    @IBOutlet private weak var favoriteView: FavoriteView!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var contentLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private(set) public var errorView: ErrorView?
    
    public var interactor: ArticleDetailInteractor!
    public weak var routingDelegate: ArticleDetailRoutingDelegate?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        favoriteView.delegate = self
        navigationItem.backBarButtonItem?.title = ""
        activityIndicator.color = Configuration.Appearance.mainColor
        interactor.requestArticleDetail()
    }
}

extension ArticleDetailViewController: ArticleDetailView, LoadingView, ErrorMessageView {
    
    func display(_ viewModel: ArticleDetailViewModel) {
        if let image = viewModel.image {
            headerImageView.image = image
        }
        dateLabel.text = viewModel.date
        titleLabel.text = viewModel.title
        contentLabel.text = viewModel.content
        favoriteView.setAsFavorite(viewModel.isFavorite)
    }
    
    func display(_ viewModel: LoadingViewModel) {
        activityIndicator.update(isLoading: viewModel.isLoading)
    }
    
    func display(_ viewModel: ErrorMessageViewModel) {
        errorView?.message = viewModel.message
    }
}

extension ArticleDetailViewController: FavoriteViewDelegate {
    func didTapFavoriteButton(in view: UIView) {
        interactor.addOrRemoveFromFavorites()
    }
}
