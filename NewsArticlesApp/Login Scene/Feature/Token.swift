//
//  Token.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 19/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public struct Token: Hashable {
    let accessToken: String
    let refreshToken: String
}
