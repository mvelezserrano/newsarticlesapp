//
//  FavoriteView.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 26/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

protocol FavoriteViewDelegate: class {
    func didTapFavoriteButton(in view: UIView)
}

public final class FavoriteView: UIView {
    weak var delegate: FavoriteViewDelegate?
    
    @IBOutlet private var button: UIButton!
    @IBOutlet private var imageView: UIImageView!
    
    public func setAsFavorite(_ isFavorite: Bool) {
        imageView.image = isFavorite ? UIImage(named: "heart_fill") : UIImage(named: "heart")
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        delegate?.didTapFavoriteButton(in: self)
    }
}
