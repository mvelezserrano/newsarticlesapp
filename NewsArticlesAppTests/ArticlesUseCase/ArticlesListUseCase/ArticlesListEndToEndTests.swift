//
//  ArticlesListEndToEndTests.swift
//  NewsArticlesAppTests
//
//  Created by Miguel Angel Vélez Serrano on 21/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import XCTest

class ArticlesListEndToEndTests: XCTestCase {
    
    func test_endToEndTestServerGETArticlesListResultWithFreshAccessToken_matchesFixedTestData() {
        _ = loginWith(user: "code", andPassword: "test")
        let expectedArticleList = expectedArticlesFromLocalJSON()
        switch getArticlesListResult() {
        case let .success(articlesList)?:
            XCTAssertEqual(articlesList.count, 5, "Expected 5 articles in the test account articles list")
            XCTAssertEqual(articlesList[0], expectedArticleList[0])
            XCTAssertEqual(articlesList[1], expectedArticleList[1])
            XCTAssertEqual(articlesList[2], expectedArticleList[2])
            XCTAssertEqual(articlesList[3], expectedArticleList[3])
            XCTAssertEqual(articlesList[4], expectedArticleList[4])
            
        case let .failure(error)?:
            XCTFail("Expected successful articles list result, got \(error) instead")
            
        default:
            XCTFail("Expected successful articles list result, got no result instead")
        }
    }
    
    // MARK: - Helpers
    
    private func getArticlesListResult(file: StaticString = #file, line: UInt = #line) -> ArticlesLoader.Result? {
        let articlesURL = URL(string: "http://amock.io/api/Mixi/v1/articles")!
        let loader = RemoteArticlesLoader(url: articlesURL, client: ephemeralClient())
        trackForMemoryLeaks(loader, file: file, line: line)
        
        let exp = expectation(description: "Wait for load completion")
        
        var receivedResult: ArticlesLoader.Result?
        loader.load { result in
            receivedResult = result
            exp.fulfill()
        }
        wait(for: [exp], timeout: 5.0)
        
        return receivedResult
    }
    
    private func loginWith(user: String, andPassword password: String,file: StaticString = #file, line: UInt = #line) -> AuthServiceProtocol.Result? {
        let authService = AuthService(client: ephemeralClient(), keychainStore: KeychainStore(service: XCTestCase.testService))
        trackForMemoryLeaks(authService, file: file, line: line)
        
        let exp = expectation(description: "Wait for login completion")
        
        var receivedResult: AuthService.Result?
        authService.loginWith(user: user, andPassword: password) { result in
            receivedResult = result
            exp.fulfill()
        }
        wait(for: [exp], timeout: 5.0)
        
        return receivedResult
    }
    
    private func expectedArticlesFromLocalJSON() -> [SummaryArticle] {
        if let url = Bundle(for: ArticlesListEndToEndTests.self).url(forResource: "ArticlesTestList", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let articlesList = try decoder.decode([ArticleDTO].self, from: data)
                return articlesList.toSummaryModels()
            } catch {
                print("error:\(error)")
            }
        }
        return []
    }
}
