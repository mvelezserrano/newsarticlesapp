//
//  ErrorMessageViewModel.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public struct ErrorMessageViewModel {
    public let message: String?
    
    static var noError: ErrorMessageViewModel {
        return ErrorMessageViewModel(message: nil)
    }

    static func error(message: String) -> ErrorMessageViewModel {
        return ErrorMessageViewModel(message: message)
    }
}
