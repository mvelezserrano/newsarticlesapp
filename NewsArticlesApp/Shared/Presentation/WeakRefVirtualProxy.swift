//
//  WeakRefVirtualProxy.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

final class WeakRefVirtualProxy<T: AnyObject> {
    private weak var object: T?
    
    init(_ object: T) {
        self.object = object
    }
}

extension WeakRefVirtualProxy: SummaryArticleView where T: SummaryArticleView {
    func display(_ viewModel: SummaryArticleViewModel) {
        object?.display(viewModel)
    }
}

extension WeakRefVirtualProxy: LoadingView where T: LoadingView {
    func display(_ viewModel: LoadingViewModel) {
        object?.display(viewModel)
    }
}

extension WeakRefVirtualProxy: ErrorMessageView where T: ErrorMessageView {
    func display(_ viewModel: ErrorMessageViewModel) {
        object?.display(viewModel)
    }
}
