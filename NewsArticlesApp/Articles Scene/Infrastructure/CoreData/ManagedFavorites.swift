//
//  ManagedFavorites.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 24/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import CoreData

@objc(ManagedFavorites)
class ManagedFavorites: NSManagedObject {
    @NSManaged var values: [Int]
}

extension ManagedFavorites {
    private static func find(in context: NSManagedObjectContext) throws -> ManagedFavorites? {
        let request = NSFetchRequest<ManagedFavorites>(entityName: entity().name!)
        request.returnsObjectsAsFaults = false
        return try context.fetch(request).first
    }
    
    static func getOrCreate(in context: NSManagedObjectContext) throws -> ManagedFavorites {
        guard let existing = try find(in: context) else {
            let new = ManagedFavorites(context: context)
            new.values = []
            try context.save()
            return new
        }
        return existing
    }
}
