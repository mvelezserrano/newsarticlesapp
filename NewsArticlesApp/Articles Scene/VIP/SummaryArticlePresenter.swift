//
//  ArticleCellPresenter.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

public protocol SummaryArticleView {
    func display(_ viewModel: SummaryArticleViewModel)
}

public class SummaryArticlePresenter {
    private let view: SummaryArticleView
    
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    public init(view: SummaryArticleView) {
        self.view = view
    }
    
    public func didStartLoadingData(for model: SummaryArticle) {
        view.display(SummaryArticleViewModel(
            date: dateFormatter.string(from: model.date),
            title: model.title,
            summary: model.summary,
            image: nil,
            isLoading: true))
    }
    
    public func didFinishLoadingData(with data: Data, for model: SummaryArticle) {
        view.display(SummaryArticleViewModel(
            date: dateFormatter.string(from: model.date),
            title: model.title,
            summary: model.summary,
            image: UIImage(data: data),
            isLoading: false))
    }
    
    public func didFinishLoadingData(with error: Error, for model: SummaryArticle) {
        view.display(SummaryArticleViewModel(
            date: dateFormatter.string(from: model.date),
            title: model.title,
            summary: model.summary,
            image: nil,
            isLoading: false))
    }
}
