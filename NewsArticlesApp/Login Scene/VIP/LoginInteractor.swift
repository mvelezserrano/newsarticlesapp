//
//  LoginInteractor.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public final class LoginInteractor {
    private let authService: AuthServiceProtocol
    private let keychainStore: KeychainStore
    
    var presenter: LoginPresenter?
    
    init(authService: AuthServiceProtocol, keychainStore: KeychainStore) {
        self.authService = authService
        self.keychainStore = keychainStore
    }
    
    func requestLoginWith(user: String, password: String) {
        presenter?.didStartLoginRequest()
        
        authService.loginWith(user: user, andPassword: password) { [weak self] result in
            switch result {
            case .success(_):
                self?.presenter?.didFinishLoginRequestWithSuccess()
            case let .failure(error as AuthService.Error):
                self?.presenter?.didFinishLoginRequest(with: error.loginPresenterError)
            default:
                assertionFailure("Invalid result")
            }
        }
    }
}

private extension AuthService.Error {
    var loginPresenterError: LoginPresenter.Error {
        switch self {
        case .unauthorized:
            return .unauthorized
        default:
            return .serverError
        }
    }
}
