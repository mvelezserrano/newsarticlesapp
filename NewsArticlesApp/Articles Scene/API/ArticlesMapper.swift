//
//  ArticlesMapper.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 21/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

final class ArticlesMapper {
    static func map(_ data: Data, from response: HTTPURLResponse) throws -> [ArticleDTO] {
        guard response.isOK, let items = try? JSONDecoder().decode([ArticleDTO].self, from: data) else {
            throw RemoteArticlesLoader.Error.invalidData
        }
        return items
    }
    
    static func mapDetail(_ data: Data, from response: HTTPURLResponse) throws -> ArticleDTO {
        guard response.isOK, let item = try? JSONDecoder().decode(ArticleDTO.self, from: data) else {
            throw RemoteDetailArticleLoader.Error.invalidData
        }
        return item
    }
}
