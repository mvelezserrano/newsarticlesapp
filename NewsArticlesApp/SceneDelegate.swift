//
//  SceneDelegate.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 19/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit
import CoreData

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var mainRouter: MainRouter?
    
    private lazy var keychainStore: KeychainStore = {
        KeychainStore(service: Configuration.Keychain.service)
    }()
    
    private lazy var httpClient: HTTPClient = {
        URLSessionHTTPClient(session: URLSession(configuration: .ephemeral), keychainStore: keychainStore)
    }()

    private lazy var favoritesStore: FavoritesStore = {
        try! CoreDataStore(storeURL: NSPersistentContainer
            .defaultDirectoryURL()
            .appendingPathComponent("articles-store.sqlite"))
    }()
    
    private lazy var authService: AuthService = {
        AuthService(client: httpClient, keychainStore: keychainStore)
    }()

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
        
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        let rootNavigationController = storyboard.instantiateInitialViewController()! as UINavigationController
        window?.rootViewController = rootNavigationController
        mainRouter = MainRouter(mainWindow: window,
                                rootNavigationController: rootNavigationController,
                                httpClient: httpClient,
                                keychainStore: keychainStore,
                                favoritesStore: favoritesStore,
                                authService: authService)
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        mainRouter?.routeToInitialScene()
    }
}

