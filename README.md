# NewsArticlesApp

[![Build Status](https://travis-ci.com/mvelezserrano/NewsArticlesApp.svg?branch=master)](https://travis-ci.com/mvelezserrano/NewsArticlesApp)

A simple list-detail app showing a list of news articles.
## Implementation details

1. **VIP architecture** inspired by [Clean Swift Architecture](https://medium.com/hackernoon/introducing-clean-swift-architecture-vip-770a639ad7bf). VC is independent of model, it communicates with Interactor (which can use a worker/loader to access model data). Interactor pass model data to presenter who is responsible for create VM with all the needed info. This VM is passed to VC to display info. VC communicates with Router using it’s own routingDelegate so VC is not responsible for navigating or deciding how/where to go next
2. **UI Implementation based on UIKit** using Storyboards
3. **Views lifecycle in UIKit and asynchronous operation** using **WeakRefVirtualProxy** and **MainQueueDispatchDecorator**
2. **Clean architecture** and performant code focused on scalability
3. **Independent modules** login and articles (list/detail)
3. **Unit and Integration tests**
4. **Not a single 3rd party library used**
5. **Persistence using Core Data**
6. **Store sensible data (token) using Keychain**. Reference: <http://www.splinter.com.au/2019/06/23/pure-swift-keychain/>
7. **No singleton used**

## API Rest
Using <http://amock.io/>

