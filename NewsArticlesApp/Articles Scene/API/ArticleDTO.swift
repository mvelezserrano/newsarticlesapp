//
//  ArticleDTO.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 21/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public struct ArticleDTO: Decodable {
    let id: Int
    let date: String
    let summary: String
    let thumbnail_template_url: String
    let thumbnail_url: String
    let title: String
    let content: String?
    let image_url: String?
    let source_url: String?
    
    func toModel() -> Article? {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-d"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        
        guard let date = formatter.date(from: date),
            let thumbnailTemplateUrl = URL(string: thumbnail_template_url),
            let thumbnailUrl = URL(string: thumbnail_url),
            let content = content,
            let image_url = image_url, let imageURL = URL(string: image_url),
            let source_url = source_url, let sourceUrl = URL(string: source_url) else {
                return nil
        }
        
        return Article(id: id,
                       date: date,
                       summary: summary,
                       thumbnailTemplateUrl: thumbnailTemplateUrl,
                       thumbnailUrl: thumbnailUrl,
                       title: title,
                       content: content,
                       imageUrl: imageURL,
                       sourceUrl: sourceUrl,
                       isFavorite: false)
    }
}
