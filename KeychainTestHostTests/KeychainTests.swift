//
//  KeychainTests.swift
//  KeychainTests
//
//  Created by Miguel Angel Vélez Serrano on 20/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import XCTest
@testable import KeychainTestHost

class KeychainTests: XCTestCase {
    
    var keychainStore: KeychainStore!
    static let testService = "KeychainTestHostTests"
    static let passwordKey = "KeychainTestsPassword"
    static let passwordKey2 = "KeychainTestsPassword2"
    
    override func setUp() {
        super.setUp()
        keychainStore = KeychainStore(service: KeychainTests.testService)
    }
    
    override class func tearDown() {
        try? KeychainStore(service: KeychainTests.testService).deleteAll()
        super.tearDown()
    }
    
    func test_keychain1_savePassword() {
        do {
            try keychainStore.set(value: "password1234", key: KeychainTests.passwordKey)
        } catch {
            XCTFail("Saving password failed with \(error.localizedDescription).")
        }
    }
    
    func test_keychain2_readPassword() {
        let password = keychainStore.get(key: KeychainTests.passwordKey)
        XCTAssertEqual("password1234", password)
    }
    
    func test_keychain3_removeAllPasswords() {
        do {
            try keychainStore.set(value: "password12345", key: KeychainTests.passwordKey2)
            try keychainStore.deleteAll()
            XCTAssertNil(keychainStore.get(key: KeychainTests.passwordKey))
            XCTAssertNil(keychainStore.get(key: KeychainTests.passwordKey2))
        } catch {
            XCTFail("Removing all password failed with \(error.localizedDescription).")
        }
    }
    
    func test_keychain4_updatePassword() {
        do {
            try keychainStore.set(value: "password1234", key: KeychainTests.passwordKey)
            try keychainStore.set(value: "password12345", key: KeychainTests.passwordKey)
            let password = keychainStore.get(key: KeychainTests.passwordKey)
            XCTAssertEqual("password12345", password)
        } catch {
            XCTFail("Update password failed with \(error.localizedDescription).")
        }
    }
}
