//
//  TokenMapper.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 20/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

struct TokenDTO: Decodable {
    let access_token: String
    let refresh_token: String
    
    func toModel() -> Token {
        return Token(accessToken: access_token, refreshToken: refresh_token)
    }
}

public final class TokenMapper {
    static func map(_ data: Data, from response: HTTPURLResponse) throws -> TokenDTO {
        guard response.created else {
            throw AuthService.Error.unauthorized
        }
        guard let token = try? JSONDecoder().decode(TokenDTO.self, from: data) else {
            throw AuthService.Error.invalidData
        }

        return token
    }
}
