//
//  LoginViewController.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

public protocol LoginRoutingDelegate: class {
    func didFinishLoginWithSuccess(inVC vc: LoginViewController)
}

public final class LoginViewController: UIViewController {
    
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    
    @IBOutlet private var textFields: [UITextField]!
    @IBOutlet private weak var loginButton: UIButton!
    
    @IBOutlet private(set) public var errorView: ErrorView?
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    public var interactor: LoginInteractor!
    public weak var routingDelegate: LoginRoutingDelegate?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        assert(routingDelegate != nil, "Routing delegate not setted")
        loginButton.setActivated(false)
        configureHideKeyboardTapGesture()
    }
    
    private func configureHideKeyboardTapGesture() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
    }
    
    @IBAction private func login(_ sender: UIButton) {
        interactor.requestLoginWith(user: usernameTextField.text!, password: passwordTextField.text!)
    }
    
    private func validateTextFields() {
        loginButton.setActivated(textFields.allSatisfy { $0.text?.isEmpty == false })
    }
    
    private func sendLoginActionIfPossible() {
        view.endEditing(true)
        guard textFields.allSatisfy ({ $0.text?.isEmpty == false }) else {
            return
        }
        login(UIButton())
    }
}

extension LoginViewController: UITextFieldDelegate {
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        validateTextFields()
        return true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        validateTextFields()
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 0 {
            passwordTextField.becomeFirstResponder()
        } else {
            sendLoginActionIfPossible()
        }
        return true
    }
}

extension LoginViewController: LoginView, LoadingView, ErrorMessageView {
    public func display(_ viewModel: LoginViewModel) {
        loginButton.setTitle(viewModel.loginMessage, for: .normal)
        loginButton.isEnabled = false
        loginButton.isHidden = true
        _ = textFields.compactMap { $0.isEnabled = false }
        routingDelegate?.didFinishLoginWithSuccess(inVC: self)
    }
    
    public func display(_ viewModel: LoadingViewModel) {
        activityIndicator?.update(isLoading: viewModel.isLoading)
        loginButton.isHidden = viewModel.isLoading
        loginButton.isEnabled = !viewModel.isLoading
    }
    
    public func display(_ viewModel: ErrorMessageViewModel) {
        self.errorView?.message = viewModel.message
    }
}

private extension UIButton {
    func setActivated(_ activated: Bool) {
        self.isEnabled = activated
        self.alpha = activated ? 1.0 : 0.5
        self.isUserInteractionEnabled = activated
    }
}
