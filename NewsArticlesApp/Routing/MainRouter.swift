//
//  MainRouter.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 26/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

final class MainRouter {
    public enum Route: Equatable {
        case launch
        case login
        case articlesList
        case articleDetail(summaryArticle: SummaryArticle)
    }
    
    private var mainWindow: UIWindow?
    
    private var httpClient: HTTPClient
    private var keychainStore: KeychainStore
    private var favoritesStore: FavoritesStore
    private var authService: AuthService
    
    private var currentNavigation: Route = .launch
    private var currentPresentedNavVC: UINavigationController?
    private var rootNavigationController: UINavigationController {
        didSet {
            mainWindow?.rootViewController = rootNavigationController
            mainWindow?.makeKeyAndVisible()
        }
    }
    
    init(mainWindow: UIWindow?, rootNavigationController: UINavigationController, httpClient: HTTPClient, keychainStore: KeychainStore, favoritesStore: FavoritesStore, authService: AuthService) {
        self.httpClient = httpClient
        self.keychainStore = keychainStore
        self.favoritesStore = favoritesStore
        self.authService = authService
        
        self.rootNavigationController = rootNavigationController
    }
    
    public func routeToInitialScene() {
        guard currentNavigation == .launch else {
            return
        }
        let destination: Route = keychainStore.userLogedIn() ? .articlesList : .login
        routeTo(destination: destination, fromOrigin: .launch)
    }
    
    public func routeTo(destination: Route, fromOrigin origin: Route) {
        assert(Thread.isMainThread)
        guard destination != origin else {
            return
        }
        switch destination {
        case .login:
            showLoginFrom(origin)
        case .articlesList:
            showArticlesListFrom(origin)
        case let .articleDetail(summaryArticle):
            showDetailForSummaryArticle(summaryArticle, origin: origin)
        default:
            break
        }
    }
    
    private func showLoginFrom(_ origin: Route) {
        let loginVC = LoginUIComposer.loginComposedWith(authService: authService, keychainStore: keychainStore)
        loginVC.routingDelegate = self
        loginVC.modalPresentationStyle = .fullScreen
        present(vc: loginVC, animated: true)
    }
    
    private func showArticlesListFrom(_ origin: Route) {
        
        let (articlesNavVC, articlesVC) = ArticlesUIComposer.articlesListComposedWith(httpClient: httpClient)
        articlesVC.routingDelegate = self
        articlesNavVC.modalPresentationStyle = .fullScreen
        switch origin {
        case .launch, .login:
            present(vc: articlesNavVC, animated: true)
            currentPresentedNavVC = articlesNavVC
        case .articleDetail:
            currentPresentedNavVC?.popViewController(animated: true)
        default:
            break
        }
        currentNavigation = .articlesList
    }
    
    private func showDetailForSummaryArticle(_ summaryArticle: SummaryArticle, origin: Route) {
        let detailVC = ArticleDetailUIComposer.articleDetailComposedWith(summaryArticle: summaryArticle, httpClient: httpClient, favoritesStore: favoritesStore)
        switch origin {
        case .articlesList:
            currentPresentedNavVC?.pushViewController(detailVC, animated: true)
        default:
            break
        }
        currentNavigation = .articleDetail(summaryArticle: summaryArticle)
    }
}

extension MainRouter {
    private func present(vc: UIViewController, animated: Bool, completion: (() -> Void)? = nil) {
        dismissAnyModal(animated: animated) { [weak self] in
            self?.rootNavigationController.present(vc, animated: true, completion: completion)
        }
    }
    
    private func dismissAnyModal(animated: Bool, completion: (() -> Void)? = nil) {
        if nil != rootNavigationController.presentedViewController {
            rootNavigationController.dismiss(animated: animated, completion: completion)
        } else {
            completion?()
        }
    }
}

extension MainRouter: LoginRoutingDelegate {
    func didFinishLoginWithSuccess(inVC vc: LoginViewController) {
        routeTo(destination: .articlesList, fromOrigin: .login)
    }
}

extension MainRouter: ArticlesListRoutingDelegate {
    func didSelect(article: SummaryArticle, inVC vc: ArticlesListViewController) {
        routeTo(destination: .articleDetail(summaryArticle: article), fromOrigin: .articlesList)
    }
    func didLogout(inVC vc: ArticlesListViewController) {
        authService.logout { [weak self] in
            self?.routeTo(destination: .login, fromOrigin: .articlesList)
        }
    }
}
