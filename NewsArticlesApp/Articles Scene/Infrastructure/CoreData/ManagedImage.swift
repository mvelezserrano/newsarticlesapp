//
//  ManagedImage.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 24/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import CoreData

@objc(ManagedImage)
class ManagedImage: NSManagedObject {
    @NSManaged var url: URL
    @NSManaged var data: Data?
}

extension ManagedImage {
    static func first(with url: URL, in context: NSManagedObjectContext) throws -> ManagedImage? {
        let request = NSFetchRequest<ManagedImage>(entityName: entity().name!)
        request.predicate = NSPredicate(format: "%K = %@", argumentArray: [#keyPath(ManagedImage.url), url])
        request.returnsObjectsAsFaults = false
        request.fetchLimit = 1
        return try context.fetch(request).first
    }
    
    static func getOrCreate(with url: URL, in context: NSManagedObjectContext) throws -> ManagedImage {
        guard let existing = try first(with: url, in: context) else {
            let new = ManagedImage(context: context)
            new.url = url
            return new
        }
        return existing
    }
}
