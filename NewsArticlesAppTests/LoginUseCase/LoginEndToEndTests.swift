//
//  LoginEndToEndTests.swift
//  NewsArticlesAppTests
//
//  Created by Miguel Angel Vélez Serrano on 20/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import XCTest

extension XCTestCase {
    static let testService = "KeychainTestService"
}

class LoginEndToEndTests: XCTestCase {
    
    func test_endToEndTestServerLoginWithCorrectUserAndPassword_returnsTokenData() {
        switch loginWith(user: "code", andPassword: "test") {
        case let .success(token):
            XCTAssertNotNil(token.accessToken)
            XCTAssertNotNil(token.refreshToken)
            
        case let .failure(error):
            XCTFail("Expected successful token result, got \(error) instead")
            
        default:
            XCTFail("Expected successful token result, got no result instead")
        }
    }
    
    // Disabled because using fixed response using http://amock.io
    func disabled_test_endToEndTestServerLoginWithIncorrectUserAndPassword_returnsError() {
        switch loginWith(user: "code", andPassword: "wrongPassword") {
        case let .success(token):
            XCTFail("Expected error result, got \(token) instead")
            
        case let .failure(error):
            XCTAssertNotNil(error)
            
        default:
            XCTFail("Expected error result, got no result instead")
        }
    }
    
    // MARK: - Helpers
    
    private func loginWith(user: String, andPassword password: String,file: StaticString = #file, line: UInt = #line) -> AuthServiceProtocol.Result? {
        let authService = AuthService(client: ephemeralClient(), keychainStore: KeychainStore(service: XCTestCase.testService))
        trackForMemoryLeaks(authService, file: file, line: line)
        
        let exp = expectation(description: "Wait for login completion")
        
        var receivedResult: AuthService.Result?
        authService.loginWith(user: user, andPassword: password) { result in
            receivedResult = result
            exp.fulfill()
        }
        wait(for: [exp], timeout: 5.0)
        
        return receivedResult
    }
}
