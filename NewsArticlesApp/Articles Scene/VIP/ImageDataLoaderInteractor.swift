//
//  ImageDataLoaderPresentationAdapter.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

public final class ImageDataLoaderInteractor {
    private let model: SummaryArticle
    private let imageDataLoader: ImageDataLoader
    private var task: ImageDataLoaderTask?

    var presenter: SummaryArticlePresenter?

    init(model: SummaryArticle, imageDataLoader: ImageDataLoader) {
        self.model = model
        self.imageDataLoader = imageDataLoader
    }

    func requestImage() {
        presenter?.didStartLoadingData(for: model)

        let model = self.model
        task = imageDataLoader.loadImageData(from: model.thumbnailUrl) { [weak self] result in
            switch result {
            case let .success(data):
                self?.presenter?.didFinishLoadingData(with: data, for: model)

            case let .failure(error):
                self?.presenter?.didFinishLoadingData(with: error, for: model)
            }
        }
    }

    func cancelImageRequest() {
        task?.cancel()
    }
}
