//
//  CoreData+FavoritesStore.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 24/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import CoreData

extension CoreDataStore: FavoritesStore {
    public func retrieve(completion: @escaping RetrievalCompletion) {
        perform { context in
            completion(Result {
                try ManagedFavorites.getOrCreate(in: context).values
            })
        }
    }
    
    
    public func insert(favoriteId: Int, completion: @escaping InsertionCompletion) {
        perform { context in
            completion(Result {
                let managedFavorites = try ManagedFavorites.getOrCreate(in: context)
                guard !managedFavorites.values.contains(favoriteId) else {
                    return
                }
                managedFavorites.values.append(favoriteId)
                try context.save()
            })
        }
    }
    
    public func delete(favoriteId: Int, completion: @escaping DeletionCompletion) {
        perform { context in
            completion(Result {
                let managedFavorites = try ManagedFavorites.getOrCreate(in: context)
                guard let existingIndex = managedFavorites.values.firstIndex(of: favoriteId) else {
                    return
                }
                managedFavorites.values.remove(at: existingIndex)
                try context.save()
            })
            
        }
    }
    
    public func isFavorite(favoriteId: Int, completion: @escaping IsFavoriteCompletion) {
        perform { context in
            completion(Result {
                try ManagedFavorites.getOrCreate(in: context).values.contains(favoriteId)
            })
        }
    }
}
