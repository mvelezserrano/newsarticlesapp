//
//  ArticleDetailUIComposer.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 26/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

public final class ArticleDetailUIComposer {
    private init() {}
    
    static func articleDetailComposedWith(summaryArticle: SummaryArticle, httpClient: HTTPClient, favoritesStore: FavoritesStore) -> ArticleDetailViewController {
        let detailURL = URL(string: String(format: "%@/%d", Configuration.APIEndpoints.articlesURL, summaryArticle.id))!
        let loader = RemoteDetailArticleLoader(url: detailURL, client: httpClient)
        let imageDataLoader = RemoteImageDataLoader(client: httpClient)
        let interactor = ArticleDetailInteractor(summaryArticle: summaryArticle, articleDetailLoader: MainQueueDispatchDecorator(decoratee: loader), imageDataLoader: MainQueueDispatchDecorator(decoratee: imageDataLoader), favoritesStore: MainQueueDispatchDecorator(decoratee: favoritesStore))
        let viewController = makeArticleDetailViewController(interactor: interactor, title: summaryArticle.title)
        
        interactor.presenter = ArticleDetailPresenter(
            articleDetailView: viewController,
            loadingView: WeakRefVirtualProxy(viewController),
            errorView: WeakRefVirtualProxy(viewController))
        
        return viewController
    }
    
    private static func makeArticleDetailViewController(interactor: ArticleDetailInteractor, title: String) -> ArticleDetailViewController {
        let bundle = Bundle(for: ArticleDetailViewController.self)
        let storyboard = UIStoryboard(name: "Articles", bundle: bundle)
        let controller = storyboard.instantiateViewController(identifier: String(describing: ArticleDetailViewController.self)) as! ArticleDetailViewController
        controller.interactor = interactor
        controller.title = title
        return controller
    }
}
