//
//  KeychainStoreSpy.swift
//  NewsArticlesAppTests
//
//  Created by Miguel Angel Vélez Serrano on 22/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

final class KeychainStoreSpy: KeychainStoreProtocol {
    var service: String
    
    init(service: String) {
        self.service = service
    }
    
    enum ReceivedMessage: Equatable {
        case set(String)
    }
    
    private(set) var receivedMessages = [ReceivedMessage]()
    
    func userLogedIn() -> Bool { return true }
    
    func getAccessToken() -> String? { return nil }
    
    func getRefreshToken() -> String? { return nil }
    
    func removeUserData(completion: @escaping (KeychainStoreResult) -> Void) { completion(.success(())) }
    
    func storeAuthDataFromLogin(user: String, password: String, andToken token: Token, completion: @escaping (KeychainStoreResult) -> Void) {
        receivedMessages.append(.set(password))
        receivedMessages.append(.set(token.accessToken))
        receivedMessages.append(.set(token.refreshToken))
        completion(.success(()))
    }
    
    func storeAuthDataFromRefreshToken(token: Token, completion: @escaping (KeychainStoreResult) -> Void) {
        receivedMessages.append(.set(token.accessToken))
        receivedMessages.append(.set(token.refreshToken))
        completion(.success(()))
    }
}
