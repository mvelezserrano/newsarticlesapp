//
//  UIActivityIndicatorView+Helpers.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 25/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import UIKit

extension UIActivityIndicatorView {
    func update(isLoading: Bool) {
        isLoading ? startAnimating() : stopAnimating()
        isHidden = !isLoading
    }
}
