//
//  CoreData+ImageDataStore.swift
//  NewsArticlesApp
//
//  Created by Miguel Angel Vélez Serrano on 24/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import Foundation

extension CoreDataStore: ImageDataStore {
    
    public func insert(_ data: Data, for url: URL, completion: @escaping (ImageDataStore.InsertionResult) -> Void) {
        perform { context in
            completion(Result {
                let managedImage = try ManagedImage.getOrCreate(with: url, in: context)
                managedImage.data = data
                try context.save()
            })
        }
    }
    
    public func retrieve(dataForURL url: URL, completion: @escaping (ImageDataStore.RetrievalResult) -> Void) {
        perform { context in
            completion(Result {
                try ManagedImage.first(with: url, in: context)?.data
            })
        }
    }
    
}
