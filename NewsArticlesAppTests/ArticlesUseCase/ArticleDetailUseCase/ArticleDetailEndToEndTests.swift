//
//  ArticleDetailEndToEndTests.swift
//  NewsArticlesAppTests
//
//  Created by Miguel Angel Vélez Serrano on 23/06/2020.
//  Copyright © 2020 MApps. All rights reserved.
//

import XCTest

class ArticleDetailEndToEndTests: XCTestCase {
    
    func test_endToEndTestServerGETArticleDetailResultWithFreshAccessToken_matchesFixedTestData() {
        _ = loginWith(user: "code", andPassword: "test")
        let expectedArticle = expectedArticleDetailFromLocalJSON()
        switch getArticleDetailResult() {
        case let .success(article)?:
            XCTAssertEqual(article, expectedArticle)
            
        case let .failure(error)?:
            XCTFail("Expected successful article result, got \(error) instead")
            
        default:
            XCTFail("Expected successful article result, got no result instead")
        }
    }
    
    // MARK: - Helpers
    
    private func getArticleDetailResult(file: StaticString = #file, line: UInt = #line) -> ArticleDetailLoader.Result? {
        let articleDetailURL = URL(string: "http://amock.io/api/Mixi/v1/articles/127")!
        let loader = RemoteDetailArticleLoader(url: articleDetailURL, client: ephemeralClient())
        trackForMemoryLeaks(loader, file: file, line: line)
        
        let exp = expectation(description: "Wait for load detail completion")
        
        var receivedResult: ArticleDetailLoader.Result?
        loader.load { result in
            receivedResult = result
            exp.fulfill()
        }
        wait(for: [exp], timeout: 5.0)
        
        return receivedResult
    }
    
    private func loginWith(user: String, andPassword password: String,file: StaticString = #file, line: UInt = #line) -> AuthServiceProtocol.Result? {
        let authService = AuthService(client: ephemeralClient(), keychainStore: KeychainStore(service: XCTestCase.testService))
        trackForMemoryLeaks(authService, file: file, line: line)
        
        let exp = expectation(description: "Wait for login completion")
        
        var receivedResult: AuthService.Result?
        authService.loginWith(user: user, andPassword: password) { result in
            receivedResult = result
            exp.fulfill()
        }
        wait(for: [exp], timeout: 5.0)
        
        return receivedResult
    }
    
    private func expectedArticleDetailFromLocalJSON() -> Article? {
        if let url = Bundle(for: ArticlesListEndToEndTests.self).url(forResource: "ArticleTestDetail", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let articleDetail = try decoder.decode(ArticleDTO.self, from: data)
                return articleDetail.toModel()
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
}
